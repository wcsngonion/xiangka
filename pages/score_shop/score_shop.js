// pages/score_shop/score_shop.js
var util = require('../../utils/util.js');

Page({

  /**
   * 页面的初始数据
   */
  data: {
    total_score: 456,
    sort: 1,
    item_list: [],

    // [
    // {
    //   "id": "60",
    //   "name": "测试用的",
    //   "img_url": "http://www.joymem.com/Public/API/image/item.png",
    //   "exchange_cost_points": "8",
    //   "exchange_cost_money": "7",
    //   "category_name": "d级"
    // },
    // {
    //   "id": "61",
    //   "name": "测试用的",
    //   "img_url": "http://www.joymem.com/Public/API/image/item.png",
    //   "exchange_cost_points": "8",
    //   "exchange_cost_money": "7",
    //   "category_name": "d级"
    // }
    // ]

    test_list: [
      {
        photo_url: "/images/abo.png",
        good_name: "这里是商品名称",
        score: 1,
        money: 0
      },
      {
        photo_url: "/images/abo.png",
        good_name: "这里是商品名称",
        score: 2,
        money: 2
      },
      {
        photo_url: "/images/abo.png",
        good_name: "这里是商品名称",
        score: 3,
        money: 0
      },
      {
        photo_url: "/images/abo.png",
        good_name: "这里是商品名称",
        score: 4,
        money: 0
      },
      {
        photo_url: "/images/abo.png",
        good_name: "这里是商品名称",
        score: 5,
        money: 0
      },
      {
        photo_url: "/images/abo.png",
        good_name: "这里是商品名称",
        score: 6,
        money: 0
      },
      {
        photo_url: "/images/abo.png",
        good_name: "这里是商品名称",
        score: 7,
        money: 0
      },
      {
        photo_url: "/images/abo.png",
        good_name: "这里是商品名称",
        score: 8,
        money: 2
      },
      {
        photo_url: "/images/abo.png",
        good_name: "这里是商品名称",
        score: 9,
        money: 2
      }
    ]
  },

  toGoodDetail: function (e) {
    var rowIdx = e.currentTarget.dataset.rowIdx
    var goodIdx = e.currentTarget.dataset.goodIdx
    var id = e.currentTarget.dataset.id
    console.log(e.currentTarget.dataset)

    wx.navigateTo({
      url: '../good_detail/good_detail?goods_id=' + id,
    })
  },

  sort_zonghe: function () {
    this.setData({
      sort: 1
    })
  },

  sort_price: function () {
    var that = this
    var sort = that.data.sort

    if (sort == 2) {
      that.setData({
        sort: 3,
        item_list: that.pack_list(that.data.test_list.reverse())
      })
    } else if (sort == 3) {
      that.setData({
        sort: 2,
        item_list: that.pack_list(that.data.test_list.reverse())
      })
    } else if (sort == 1) {
      that.setData({
        sort: 2
      })
    }

  },

  pack_list: function (list) {
    var ret = []

    for (var i = 0; i < list.length; i += 2) {
      if (i == list.length - 1) {
        ret.push([list[i]])
      } else {
        ret.push([list[i], list[i + 1]])
      }
    }

    return ret
  },

  getScore: function () {
    var that = this
    wx.navigateTo({
      url: '../get_score/get_score?total_score=' + that.data.total_score,
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // var that = this
    // var test_list = that.data.test_list

    // var item_list_temp = that.pack_list(test_list)
    // that.setData({
    //   item_list: item_list_temp
    // })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that = this

    //获取积分商城商品
    wx.request({
      url: getApp().globalData.server + "/API/Goods/get_all_allow_exchange_goods",
      data: {
        company_id: getApp().globalData.settings.company_id,
        shop_id: getApp().globalData.settings.shop_id,
      },
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function (res2) {
        console.log(res2)
        if (res2.data.error_no == 10008) {
          that.setData({
            total_score: getApp().globalData.userInfo_detail.point
          })
        }else if (res2.data.error_no != 0) {
          wx.showModal({
            title: '哎呀',
            content: '获取积分商城商品失败' + res2.data.data.error_msg,
            success: function (res) {
              if (res.confirm) {
                console.log('用户点击确定')
              } else if (res.cancel) {
                console.log('用户点击取消')
              }
            }
          })
        }
        else if (res2.data.error_no == 0) {
          that.setData({
            item_list: that.pack_list(res2.data.data),
            total_score: getApp().globalData.userInfo_detail.point
          })
        }
      },
      fail: function (res) {
        wx.showModal({
          title: '哎呀～',
          content: '获取积分商城商品失败',
          success: function (res) {
            if (res.confirm) {
              console.log('用户点击确定')
            } else if (res.cancel) {
              console.log('用户点击取消')
            }
          }
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return (util.share())
  }
})
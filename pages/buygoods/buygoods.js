// pages/buygoods/buygoods.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    buydata: {},
    receiver: "",
    telnumber: "",
    address: "",
    message: "",
    company_name: getApp().globalData.settings.company_name,
  },

  truebuy: function(e) {
    var that = this
    var myreg = /^(((13[0-9]{1})|(15[0-9]{1})|(18[0-9]{1})|(17[0-9]{1}))+\d{8})$/;
    if (that.data.receiver == '') {
      wx.showModal({
        title: '提示！',
        showCancel: false,
        content: '请输入收件人！',
        success: function(res) {}
      })
    } else if (that.data.telnumber == '') {
      wx.showModal({
        title: '提示！',
        showCancel: false,
        content: '请输入手机号！',
        success: function(res) {}
      })
    } else if (that.data.telnumber.length != 11) {
      wx.showModal({
        title: '提示！',
        showCancel: false,
        content: '手机号长度有误，请重新输入！',
        success: function(res) {}
      })
    } else if (!myreg.test(that.data.telnumber)) {
      wx.showModal({
        title: '提示！',
        showCancel: false,
        content: '请输入正确的手机号码（仅支持中国大陆）！',
        success: function(res) {}
      })
    } else if (that.data.address == '') {
      wx.showModal({
        title: '提示！',
        showCancel: false,
        content: '请输入收货地址！',
        success: function(res) {}
      })
    } else {
      var that = this
      // console.log(getApp().globalData.userInfo)  
      wx.login({
        success: function(res) {
          if (res.code) {
            console.log(res.code)

            var goods_arr_tmp = []
            var goods = that.data.buydata.goods
            for (var i = 0; i < goods.length; i++) {
              var temp = {
                goods_type: goods[i].gooddetail.type,
                goods_id: goods[i].goodid,
                goods_name: goods[i].gooddetail.name,
                unit_price: goods[i].gooddetail.unit_price,
                original_price: goods[i].gooddetail.original_price,
                photo_url: goods[i].gooddetail.img_url,
                discount: goods[i].gooddetail.discount,
                quantity: goods[i].numb,
                note: "商品购买",
                company_id: getApp().globalData.settings.company_id,
                shop_id: getApp().globalData.settings.shop_id
              }
              goods_arr_tmp.push(temp)
            }
            console.log('商品信息', goods_arr_tmp)

            var temp_receiver = {
              receiver_username: that.data.receiver,
              receiver_addr: that.data.address,
              receiver_phone: that.data.telnumber,
            }
            console.log('收件人信息', temp_receiver)

            // console.log(_order_type)
            wx.request({
              url: getApp().globalData.server + '/API/Shopping/do_order',
              data: {
                shop_id: getApp().globalData.settings.shop_id,
                company_id: getApp().globalData.settings.company_id,
                shop_name: getApp().globalData.settings.shop_name,
                user_id: getApp().globalData.userInfo_detail.user_id,
                username: getApp().globalData.userInfo_detail.username,
                note: that.data.message,
                order_type: 1,
                receiver: JSON.stringify(temp_receiver),
                goods_arr: JSON.stringify(goods_arr_tmp),
              },
              method: "POST",
              header: {
                "Content-Type": "application/x-www-form-urlencoded"
              },
              success: function(res1) {
                console.log(res1)
                if (res1.data.error_no != 0) {
                  wx.showModal({
                    title: '哎呀～',
                    content: '创建订单失败！',
                    success: function(res) {
                      if (res.confirm) {
                        console.log('用户点击确定')
                      } else if (res.cancel) {
                        console.log('用户点击取消')
                      }
                    }
                  })
                } else if (res1.data.error_no == 0) {
                  if (res1.data.data.total_to_pay_money == 0) {
                    var goods = that.data.buydata.goods
                    var carts = getApp().globalData.yi_shopinglist
                    var nlist = []
                    for (var i = 0; i < carts.length; i++) {
                      var tmp = 0
                      for (var j = 0; j < goods.length; j++) {
                        if (goods[j].goodid == carts[i].goodid) {
                          tmp = 1
                        }
                      }
                      if (tmp == 0) {
                        nlist.push(carts[i])
                      }
                    }
                    getApp().globalData.yi_shopinglist = nlist
                    wx.showModal({
                      title: '恭喜',
                      showCancel: false,
                      content: '支付成功！',
                      success: function(res) {

                      },
                      complete: function(res) {
                        wx.reLaunch({
                          url: '../myorder/myorder'
                        })
                      }
                    })
                  } else {
                    wx.request({
                      url: getApp().globalData.server + "/API/Pay/pay_request",
                      data: {
                        code: res.code,
                        company_id: getApp().globalData.settings.company_id,
                        shop_id: getApp().globalData.settings.shop_id,
                        order_id: res1.data.data.order_id,
                        total_to_pay_money: res1.data.data.total_to_pay_money,
                        user_id: getApp().globalData.userInfo_detail.user_id
                      },
                      method: "POST",
                      header: {
                        "Content-Type": "application/x-www-form-urlencoded"
                      },
                      success: function(res2) {
                        console.log(res2)
                        if (res2.data.error_no != 0) {
                          wx.showModal({
                            title: '哎呀',
                            content: '请求支付失败！' + res2.data.data.error_msg,
                            success: function(res) {
                              if (res.confirm) {
                                console.log('用户点击确定')
                              } else if (res.cancel) {
                                console.log('用户点击取消')
                              }
                            }
                          })
                        } else if (res2.data.error_no == 0) {
                          wx.requestPayment({
                            'timeStamp': res2.data.data.timeStamp + '',
                            'nonceStr': res2.data.data.nonceStr,
                            'package': res2.data.data.package,
                            'signType': 'MD5',
                            'paySign': res2.data.data.paySign,
                            'success': function(res) {
                              console.log(res)
                              if (res.errMsg == "requestPayment:ok") {
                                var goods = that.data.buydata.goods
                                var carts = getApp().globalData.yi_shopinglist
                                var nlist = []
                                for (var i = 0; i < carts.length; i++) {
                                  var tmp = 0
                                  for (var j = 0; j < goods.length; j++) {
                                    if (goods[j].goodid == carts[i].goodid) {
                                      tmp = 1
                                    }
                                  }
                                  if (tmp == 0) {
                                    nlist.push(carts[i])
                                  }
                                }
                                getApp().globalData.yi_shopinglist = nlist
                                wx.showModal({
                                  title: '恭喜',
                                  showCancel: false,
                                  content: '支付成功！',
                                  success: function(res) {

                                  },
                                  complete: function(res) {
                                    wx.reLaunch({
                                      url: '../myorder/myorder'
                                    })
                                  }
                                })
                              } else {
                                wx.showModal({
                                  title: '哎呀',
                                  content: '支付失败！',
                                  success: function(res) {
                                    if (res.confirm) {
                                      console.log('用户点击确定')
                                    } else if (res.cancel) {
                                      console.log('用户点击取消')
                                    }
                                  }
                                })
                              }
                            },
                            'fail': function(res) {
                              console.log(res)
                              wx.showModal({
                                title: '哎呀',
                                content: '支付失败！',
                                success: function(res) {
                                  if (res.confirm) {
                                    console.log('用户点击确定')
                                  } else if (res.cancel) {
                                    console.log('用户点击取消')
                                  }
                                }
                              })
                            },
                            'complete': function(ßres) {}
                          })
                        }
                      },
                      fail: function(res) {
                        wx.showModal({
                          title: '哎呀～',
                          content: '请求支付失败！',
                          success: function(res) {
                            if (res.confirm) {
                              console.log('用户点击确定')
                            } else if (res.cancel) {
                              console.log('用户点击取消')
                            }
                          }
                        })
                      }
                    })
                  }
                }
              },
              fail: function(res) {
                wx.showModal({
                  title: '哎呀～',
                  content: '网络不在状态呢！',
                  success: function(res) {
                    if (res.confirm) {
                      console.log('用户点击确定')
                    } else if (res.cancel) {
                      console.log('用户点击取消')
                    }
                  }
                })
              }
            })


          } else {
            console.log('登录失败！' + res.errMsg)
            wx.showModal({
              title: '哎呀～',
              content: '内部错误1',
              success: function(res) {
                if (res.confirm) {
                  console.log('用户点击确定')
                } else if (res.cancel) {
                  console.log('用户点击取消')
                }
              }
            })
          }
        }
      });
    }
  },

  receiverInput: function(e) {
    this.data.receiver = e.detail.value
  },

  telnumberInput: function(e) {
    this.data.telnumber = e.detail.value
  },

  addressInput: function(e) {
    this.data.address = e.detail.value
  },

  messageInput: function(e) {
    this.data.message = e.detail.value
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.setData({
      buydata: getApp().globalData.yi_buylist,
      company_name: getApp().globalData.settings.company_name,
    })
    //console.log(this.data)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {
    this.setData({
      buydata: getApp().globalData.yi_buylist,
      company_name: getApp().globalData.settings.company_name,
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    this.setData({
      buydata: getApp().globalData.yi_buylist,
      company_name: getApp().globalData.settings.company_name,
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})
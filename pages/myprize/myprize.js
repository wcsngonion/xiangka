// pages/myprize/myprize.js
var app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    show_data: [],
    indicatorDots: true,
    autoplay: true,
    interval: 5000,
    duration: 1000,
    userInfo: {},
    userListInfo: [{
        icon: '/images/jifen.png',
        text: '签到奖励五积分',
        text2: '数量不限',
        isunread: true,
        unreadNum: 2
      },
      // { icon: '/images/quan.jpg',
      //   text: '先锋耳机立减优惠券',
      //   text2: '限量：1000份',
      //   isunread: true,
      //   unreadNum: 2
      // }
    ],
    userListInfo4: [{
        icon: '/images/jifen.png',
        text: '签到奖励十积分',
        text2: '数量不限',
        isunread: false,
        unreadNum: 2
      },
      {
        icon: '/images/huiyuanka.png',
        text: '次卡一次',
        text2: '限量500份',
        isunread: false,
        unreadNum: 2
      }
    ],
    userListInfo7: [{
        icon: '/images/jifen.png',
        text: '签到奖励二十积分',
        text2: '数量不限',
        isunread: false,
        unreadNum: 2
      },
      {
        icon: '/images/huiyuanka.png',
        text: '次卡三次',
        text2: '限量500份',
        isunread: false,
        unreadNum: 2
      },
      {
        icon: '/images/cup.png',
        text: '水杯0元购',
        text2: '限量500份',
        isunread: false,
        unreadNum: 2
      }
    ],
    guize: '  一 签到周期：周一00:00:00至周日00:00:00为一个签到周期，一周内用户累计签满三天和七天均可领取奖品。\n  二 领奖时间：领奖时间开始为第三天和第七天的10:00:00，领奖结束时间为第七天的23:59:59 '
  },

  timestampToTime: function(timestamp) {
    var date = new Date(timestamp * 1000); //时间戳为10位需*1000，时间戳为13位的话不需乘1000
    var Y = date.getFullYear() + '-';
    var M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-';
    var D = date.getDate() + ' ';
    var h = date.getHours() + ':';
    var m = date.getMinutes() + ':';
    var s = date.getSeconds();
    //console.log(Y + M + D + h + m + s)
    return Y + M + D + h + m + s;
  },

  formSubmit: function(e) {
    var that = this;

    wx.showModal({
      title: '提示！',
      content: '确定要领取该奖品吗？',
      success: function(res) {
        if (res.confirm) {
          console.log('用户点击确定')
          const idx = e.detail.value.idx;
          console.log('奖品id：', idx);

          //确认领奖
          wx.request({
            url: getApp().globalData.server + '/API/Lottery/confirm_award_prize',
            data: {
              shop_id: getApp().globalData.settings.shop_id,
              company_id: getApp().globalData.settings.company_id,
              user_id: getApp().globalData.userInfo_detail.user_id,
              shop_name: getApp().globalData.settings.shop_name,
              username: getApp().globalData.userInfo_detail.username,
              prize_log_id: idx,
            },
            method: "POST",
            header: {
              "Content-Type": "application/x-www-form-urlencoded"
            },
            success: function(res) {
              console.log('确认领奖', res.data)
              if (res.data.error_no != 0) {
                wx.showModal({
                  title: '哎呀～',
                  content: '出错了呢！' + res.data.data.error_msg,
                  success: function(res) {
                    if (res.confirm) {
                      console.log('用户点击确定')
                    } else if (res.cancel) {
                      console.log('用户点击取消')
                    }
                  }
                })
              } else if (res.data.error_no == 0) {
                //按钮状态重置
                for (var i = 0; i < that.data.show_data.length; i++) {
                  if (that.data.show_data[i].id == idx) {
                    var tmp = 'show_data[' + i + '].state'
                    that.setData({
                      [tmp]: '1',
                    })
                  }
                }

                //积分变动服务提醒
                if (res.data.data.to_add_goods_type == 6) {
                  console.log('积分变动微信提醒(formId)：', e.detail.formId)
                  //积分变动微信提醒
                  wx.request({
                    url: getApp().globalData.server + '/API/Point/point_wechat_notify',
                    data: {
                      company_id: getApp().globalData.settings.company_id,
                      shop_id: getApp().globalData.settings.shop_id,
                      user_id: getApp().globalData.userInfo_detail.user_id,
                      username: getApp().globalData.userInfo_detail.username,
                      openid: getApp().globalData.open_id,
                      form_id: e.detail.formId,
                      points_type: '新增',
                      points_from: '领取抽奖奖品',
                      consume_points: res.data.data.has_change_point,
                      total_points: res.data.data.current_point,
                    },
                    method: "POST",
                    header: {
                      "Content-Type": "application/x-www-form-urlencoded"
                    },
                    success: function(res) {
                      console.log('积分变动微信提醒', res.data)
                      if (res.data.error_no != 0) {
                        // wx.showModal({
                        //   title: '哎呀～',
                        //   content: '出错了呢！' + res.data.data.error_msg,
                        //   success: function (res) {
                        //     if (res.confirm) {
                        //       console.log('用户点击确定')
                        //     } else if (res.cancel) {
                        //       console.log('用户点击取消')
                        //     }
                        //   }
                        // })
                      } else if (res.data.error_no == 0) {}
                    },
                    fail: function(res) {
                      // wx.showModal({
                      //   title: '哎呀～',
                      //   content: '网络不在状态呢！',
                      //   success: function (res) {
                      //     if (res.confirm) {
                      //       console.log('用户点击确定')
                      //     } else if (res.cancel) {
                      //       console.log('用户点击取消')
                      //     }
                      //   }
                      // })
                    }
                  })
                }

                //领取成功弹框
                wx.showModal({
                  title: '恭喜！',
                  content: res.data.data.tip,
                  showCancel: false,
                  success: function(res2) {
                    if (res2.confirm) {
                      console.log('用户点击确定')
                      if (res.data.data.to_add_goods_type == 1) {
                        wx.navigateTo({
                          url: '../myorder/myorder?tab=1'
                        })
                      } else if (res.data.data.to_add_goods_type == 3) {
                        getApp().globalData.userInfo_detail.second_balance = res.data.data.second_balance,
                          wx.reLaunch({
                            url: '../mine/mine'
                          })
                      } else if (res.data.data.to_add_goods_type == 4) {
                        wx.navigateTo({
                          url: '../my_list/my_list'
                        })
                      } else if (res.data.data.to_add_goods_type == 6) {
                        getApp().globalData.userInfo_detail.point = res.data.data.current_point
                        wx.reLaunch({
                          url: '../mine/mine'
                        })
                      }
                    } else if (res2.cancel) {
                      console.log('用户点击取消')
                    }
                  },
                })
              }
            },
            fail: function(res) {
              wx.showModal({
                title: '哎呀～',
                content: '网络不在状态呢！',
                success: function(res) {
                  if (res.confirm) {
                    console.log('用户点击确定')
                  } else if (res.cancel) {
                    console.log('用户点击取消')
                  }
                }
              })
            }
          })
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var that = this

    //获取我的中奖列表
    wx.request({
      url: getApp().globalData.server + '/API/Lottery/get_all_award_prize',
      data: {
        shop_id: getApp().globalData.settings.shop_id,
        company_id: getApp().globalData.settings.company_id,
        user_id: getApp().globalData.userInfo_detail.user_id,
      },
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function(res) {
        console.log('获取我的中奖列表', res.data)
        if (res.data.error_no != 0) {
          wx.showModal({
            title: '哎呀～',
            content: '出错了呢！' + res.data.data.error_msg,
            success: function(res) {
              if (res.confirm) {
                console.log('用户点击确定')
              } else if (res.cancel) {
                console.log('用户点击取消')
              }
            }
          })
        } else if (res.data.error_no == 0) {
          var sdata = res.data.data
          for (var i = 0; i < sdata.length; i++) {
            var ctime = that.timestampToTime(sdata[i].create_timestamp);
            //console.log(ctime)
            sdata[i].create_timestamp = ctime
          }
          that.setData({
            show_data: sdata,
          })
        }
      },
      fail: function(res) {
        wx.showModal({
          title: '哎呀～',
          content: '网络不在状态呢！',
          success: function(res) {
            if (res.confirm) {
              console.log('用户点击确定')
            } else if (res.cancel) {
              console.log('用户点击取消')
            }
          }
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})
// pages/clock/clock.js
var util = require('../../utils/util.js');
let chooseYear = null;
let chooseMonth = null;

Page({

  /**
   * 页面的初始数据
   */
  data: {
    hasEmptyGrid: false,
    showPicker: false,

    history_sign_times: '0',
    continuous_sign_times: '0',
    longest_sign_times: '0',
    sign_days_timestamp: [],
    today_is_sign: false,
    sign_tip: '',
    pic_reward: '/images/gift_1.png',
    unreward: true,
  },

  getclockinfo: function(_year, _month, _days) {
    var that = this

    var year = _year.toString()
    var month = _month.toString()
    var days = _days.toString()

    var _start = '',
      _end = ''
    _start += year
    _end += year
    if (month.length == 1) {
      _start += '0'
      _end += '0'
    }
    _start += month + '01'
    _end += month + days

    console.log(_start, _end)
    //获取用户签到信息
    wx.request({
      url: getApp().globalData.server + '/API/Sign/get_sign_info',
      data: {
        shop_id: getApp().globalData.settings.shop_id,
        company_id: getApp().globalData.settings.company_id,
        user_id: getApp().globalData.userInfo_detail.user_id,
        start_date: _start,
        end_date: _end,
      },
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function(res) {
        console.log('获取用户签到信息', res.data)
        if (res.data.error_no != 0) {
          if (res.data.error_no == 10008) {
            that.setData({
              history_sign_times: '0',
              continuous_sign_times: '0',
              longest_sign_times: '0',
              today_is_sign: false,
            })
          } else {
            wx.showModal({
              title: '哎呀～',
              content: '出错了呢！' + res.data.data.error_msg,
              success: function(res) {
                if (res.confirm) {
                  console.log('用户点击确定')
                } else if (res.cancel) {
                  console.log('用户点击取消')
                }
              }
            })
          }
        } else if (res.data.error_no == 0) {
          that.setData({
            history_sign_times: res.data.data.history_sign_times,
            continuous_sign_times: res.data.data.continuous_sign_times,
            longest_sign_times: res.data.data.longest_sign_times,
            sign_days_timestamp: res.data.data.sign_days_timestamp,
            today_is_sign: res.data.data.today_is_sign,
          })

          var timestamp = res.data.data.sign_days_timestamp;
          var num = timestamp.length;
          for (var i = 0; i <= num - 1; i++) {
            var _timestamp = timestamp[i]
            var date = new Date(_timestamp * 1000);
            var idx = date.getDate() - 1
            var days = that.data.days;
            //console.log(_timestamp,date,idx,days)
            days[idx].choosed = !days[idx].choosed;
            //console.log(idx)
            that.setData({
              days,
            });
          }

          if (res.data.data.today_is_sign == 1) {
            var tmp = that.timestampToTime(res.data.data.today_sign_timestamp)
            that.setData({
              sign_tip: '< 签到时间：' + tmp + ' >',
            });
          }
        }
      },
      fail: function(res) {
        wx.showModal({
          title: '哎呀～',
          content: '网络不在状态呢！',
          success: function(res) {
            if (res.confirm) {
              console.log('用户点击确定')
            } else if (res.cancel) {
              console.log('用户点击取消')
            }
          }
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    // wx.request({
    //   url: getApp().globalData.server + '/API/Sign/delete_all_record',
    //   data: {
    //     shop_id: getApp().globalData.settings.shop_id,
    //     company_id: getApp().globalData.settings.company_id,
    //     user_id: getApp().globalData.userInfo_detail.user_id,
    //   },
    //   method: "POST",
    //   header: {
    //     "Content-Type": "application/x-www-form-urlencoded"
    //   },
    //   success: function (res) {
    //     console.log(res)
    //   },
    //   fail: function (res) {
    //     console.log(res)
    //   }
    // })

    const date = new Date();
    const curYear = date.getFullYear();
    const curMonth = date.getMonth() + 1;
    const weeksCh = ['日', '一', '二', '三', '四', '五', '六'];
    this.calculateEmptyGrids(curYear, curMonth);
    this.calculateDays(curYear, curMonth);
    this.setData({
      curYear,
      curMonth,
      weeksCh
    });
    //console.log(this.data.curYear, this.data.curMonth, this.data.days.length)

    this.getclockinfo(this.data.curYear, this.data.curMonth, this.data.days.length)
  },

  formSubmit: function(e) {
    var that = this
    if (that.data.today_is_sign == true) {
      wx.showModal({
        title: '哎呀～',
        content: '您已经签到了呢！',
        showCancel: false,
        success: function(res) {
          if (res.confirm) {
            console.log('用户点击确定')
          } else if (res.cancel) {
            console.log('用户点击取消')
          }
        }
      })
    } else {
      //用户签到
      wx.request({
        url: getApp().globalData.server + '/API/Sign/user_sign',
        data: {
          shop_id: getApp().globalData.settings.shop_id,
          company_id: getApp().globalData.settings.company_id,
          user_id: getApp().globalData.userInfo_detail.user_id,
        },
        method: "POST",
        header: {
          "Content-Type": "application/x-www-form-urlencoded"
        },
        success: function(res) {
          console.log('用户签到', res.data)
          if (res.data.error_no != 0) {
            if (res.data.error_no == 10009) {
              wx.showModal({
                title: '哎呀～',
                content: '您已经签到了呢！',
                showCancel: false,
                success: function(res) {
                  if (res.confirm) {
                    console.log('用户点击确定')
                  } else if (res.cancel) {
                    console.log('用户点击取消')
                  }
                }
              })
            } else {
              wx.showModal({
                title: '哎呀～',
                content: '出错了呢！' + res.data.data.error_msg,
                success: function(res) {
                  if (res.confirm) {
                    console.log('用户点击确定')
                  } else if (res.cancel) {
                    console.log('用户点击取消')
                  }
                }
              })
            }
          } else if (res.data.error_no == 0) {
            getApp().globalData.userInfo_detail.point = res.data.data.current_point

            that.setData({
              history_sign_times: res.data.data.history_sign_times,
              continuous_sign_times: res.data.data.continuous_sign_times,
              longest_sign_times: res.data.data.longest_sign_times,
              today_is_sign: true,
            })

            var tmp = that.timestampToTime(res.data.data.today_sign_timestamp)
            var date = new Date(res.data.data.today_sign_timestamp * 1000);
            var idx = date.getDate() - 1
            var days = that.data.days;
            //console.log(tmp,date,idx,days)
            days[idx].choosed = !days[idx].choosed;
            //console.log(idx)
            that.setData({
              days,
              sign_tip: '< 签到时间：' + tmp + ' >',
            });

            console.log('积分变动微信提醒(formId)：', e.detail.formId)
            //积分变动微信提醒
            wx.request({
              url: getApp().globalData.server + '/API/Point/point_wechat_notify',
              data: {
                company_id: getApp().globalData.settings.company_id,
                shop_id: getApp().globalData.settings.shop_id,
                user_id: getApp().globalData.userInfo_detail.user_id,
                username: getApp().globalData.userInfo_detail.username,
                openid: getApp().globalData.open_id,
                form_id: e.detail.formId,
                points_type: '新增',
                points_from: '签到',
                consume_points: res.data.data.add_point,
                total_points: res.data.data.current_point,
              },
              method: "POST",
              header: {
                "Content-Type": "application/x-www-form-urlencoded"
              },
              success: function(res) {
                console.log('积分变动微信提醒', res.data)
                if (res.data.error_no != 0) {
                  // wx.showModal({
                  //   title: '哎呀～',
                  //   content: '出错了呢！' + res.data.data.error_msg,
                  //   success: function (res) {
                  //     if (res.confirm) {
                  //       console.log('用户点击确定')
                  //     } else if (res.cancel) {
                  //       console.log('用户点击取消')
                  //     }
                  //   }
                  // })
                } else if (res.data.error_no == 0) {}
              },
              fail: function(res) {
                // wx.showModal({
                //   title: '哎呀～',
                //   content: '网络不在状态呢！',
                //   success: function (res) {
                //     if (res.confirm) {
                //       console.log('用户点击确定')
                //     } else if (res.cancel) {
                //       console.log('用户点击取消')
                //     }
                //   }
                // })
              }
            })

            wx.showModal({
              title: '恭喜',
              content: '签到成功！' + res.data.data.tips,
              showCancel: false,
              success: function(res) {
                if (res.confirm) {
                  console.log('用户点击确定')
                } else if (res.cancel) {
                  console.log('用户点击取消')
                }
              }
            })
          }
        },
        fail: function(res) {
          wx.showModal({
            title: '哎呀～',
            content: '网络不在状态呢！',
            success: function(res) {
              if (res.confirm) {
                console.log('用户点击确定')
              } else if (res.cancel) {
                console.log('用户点击取消')
              }
            }
          })
        }
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {
    return (util.share())
  },

  getThisMonthDays(year, month) {
    return new Date(year, month, 0).getDate();
  },
  getFirstDayOfWeek(year, month) {
    return new Date(Date.UTC(year, month - 1, 1)).getDay();
  },
  calculateEmptyGrids(year, month) {
    const firstDayOfWeek = this.getFirstDayOfWeek(year, month);
    let empytGrids = [];
    if (firstDayOfWeek > 0) {
      for (let i = 0; i < firstDayOfWeek; i++) {
        empytGrids.push(i);
      }
      this.setData({
        hasEmptyGrid: true,
        empytGrids
      });
    } else {
      this.setData({
        hasEmptyGrid: false,
        empytGrids: []
      });
    }
  },
  calculateDays(year, month) {
    let days = [];

    const thisMonthDays = this.getThisMonthDays(year, month);

    for (let i = 1; i <= thisMonthDays; i++) {
      days.push({
        day: i,
        choosed: false
      });
    }

    this.setData({
      days
    });
  },
  handleCalendar(e) {
    const handle = e.currentTarget.dataset.handle;
    const curYear = this.data.curYear;
    const curMonth = this.data.curMonth;
    if (handle === 'prev') {
      let newMonth = curMonth - 1;
      let newYear = curYear;
      if (newMonth < 1) {
        newYear = curYear - 1;
        newMonth = 12;
      }

      this.calculateDays(newYear, newMonth);
      this.calculateEmptyGrids(newYear, newMonth);

      this.setData({
        curYear: newYear,
        curMonth: newMonth
      });
    } else {
      let newMonth = curMonth + 1;
      let newYear = curYear;
      if (newMonth > 12) {
        newYear = curYear + 1;
        newMonth = 1;
      }

      this.calculateDays(newYear, newMonth);
      this.calculateEmptyGrids(newYear, newMonth);

      this.setData({
        curYear: newYear,
        curMonth: newMonth
      });
    }
    console.log(this.data.curYear, this.data.curMonth)
    this.getclockinfo(this.data.curYear, this.data.curMonth, this.data.days.length)
  },

  //显示选中日期
  tapDayItem(e) {
    const idx = e.currentTarget.dataset.idx;
    const days = this.data.days;
    days[idx].choosed = !days[idx].choosed;
    console.log(idx)
    this.setData({
      days,
    });
  },

  chooseYearAndMonth() {
    const curYear = this.data.curYear;
    const curMonth = this.data.curMonth;
    let pickerYear = [];
    let pickerMonth = [];
    for (let i = 1900; i <= 2100; i++) {
      pickerYear.push(i);
    }
    for (let i = 1; i <= 12; i++) {
      pickerMonth.push(i);
    }
    const idxYear = pickerYear.indexOf(curYear);
    const idxMonth = pickerMonth.indexOf(curMonth);
    this.setData({
      pickerValue: [idxYear, idxMonth],
      pickerYear,
      pickerMonth,
      showPicker: true,
    });
  },
  pickerChange(e) {
    const val = e.detail.value;
    chooseYear = this.data.pickerYear[val[0]];
    chooseMonth = this.data.pickerMonth[val[1]];
  },
  tapPickerBtn(e) {
    const type = e.currentTarget.dataset.type;
    const o = {
      showPicker: false,
    };
    if (type === 'confirm') {
      o.curYear = chooseYear;
      o.curMonth = chooseMonth;
      this.calculateEmptyGrids(chooseYear, chooseMonth);
      this.calculateDays(chooseYear, chooseMonth);
    }

    this.setData(o);
    console.log(this.data.curYear, this.data.curMonth)
    this.getclockinfo(this.data.curYear, this.data.curMonth, this.data.days.length)
  },
  timestampToTime: function(timestamp) {
    var date = new Date(timestamp * 1000); //时间戳为10位需*1000，时间戳为13位的话不需乘1000
    var Y = date.getFullYear() + '-';
    var M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-';
    var D = date.getDate() + ' ';
    var h = date.getHours() + ':';
    var m = date.getMinutes() + ':';
    var s = date.getSeconds();
    //console.log(Y + M + D + h + m + s)
    return Y + M + D + h + m + s;
  },
})
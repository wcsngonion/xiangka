// pages/exshop/exshop.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    currentTab: 0,
    industries: [],
    shops: [],
  },

  changeshop: function(e) {
    console.log('更改店铺的参数：', e.currentTarget.dataset)

    //切换店铺
    wx.request({
      url: getApp().globalData.server + '/API/Shop/change_shop',
      data: {
        to_shop_id: e.currentTarget.dataset.sid,
        company_id: e.currentTarget.dataset.cid,
        user_id: getApp().globalData.userInfo_detail.user_id,
      },
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function(res) {
        console.log('切换店铺：', res.data)
        if (res.data.error_no != 0) {
          wx.showModal({
            title: '哎呀～',
            content: '出错了呢！' + res.data.data.error_msg,
            success: function(res) {
              if (res.confirm) {
                console.log('用户点击确定')
              } else if (res.cancel) {
                console.log('用户点击取消')
              }
            }
          })
        } else if (res.data.error_no == 0) {
          wx.reLaunch({
            url: '../load/load'
          })
        }
      },
      fail: function(res) {
        wx.showModal({
          title: '哎呀～',
          content: '网络不在状态呢！',
          success: function(res) {
            if (res.confirm) {
              console.log('用户点击确定')
            } else if (res.cancel) {
              console.log('用户点击取消')
            }
          }
        })
      }
    })
  },

  swichNav: function(e) {
    var that = this
    var index = e.currentTarget.dataset.current

    that.setData({
      currentTab: index
    })

    that.get_shops(that.data.industries[index].industry_id)
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var that = this

    //得到该商圈cbd需要展示的日期和行业分类
    wx.request({
      url: getApp().globalData.server + "/API/Shop/get_to_show_days_and_industries_of_one_cbd",
      data: {
        company_id: getApp().globalData.settings.company_id
      },
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function(res1) {
        console.log('得到该商圈cbd需要展示的日期和行业分类：', res1)
        if (res1.data.error_no == 0) {

          that.setData({
            industries: res1.data.data.industries,
          })

          that.get_shops(res1.data.data.industries[0].industry_id)

        } else {
          wx.showModal({
            title: '哎呀～',
            content: '得到该商圈cbd需要展示的日期和行业分类失败',
            success: function(res1) {
              if (res1.confirm) {
                console.log('用户点击确定')
              } else if (res1.cancel) {
                console.log('用户点击取消')
              }
            }
          })
        }
      },
      fail: function(res) {
        wx.showModal({
          title: '哎呀～',
          content: '连接失败了',
          success: function(res) {
            if (res.confirm) {
              console.log('用户点击确定')
            } else if (res.cancel) {
              console.log('用户点击取消')
            }
          }
        })
        console.log("fail!", res)
      },
      complete: function(res) {

      }
    })
  },

  get_shops: function(industry_id) {
    var that = this

    //得到指定公司的所有店铺
    wx.request({
      url: getApp().globalData.server + "/API/Shop/get_all_shop_of_one_company",
      data: {
        company_id: getApp().globalData.settings.company_id,
        industry_id: industry_id
      },
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function(res2) {
        console.log('得到指定公司的所有店铺：', res2)
        if (res2.data.error_no == 0) {

          that.setData({
            shops: res2.data.data.shops
          })

        } else {
          wx.showModal({
            title: '哎呀～',
            content: '得到指定公司的所有店铺失败',
            success: function(res) {
              if (res.confirm) {
                console.log('用户点击确定')
              } else if (res.cancel) {
                console.log('用户点击取消')
              }
            }
          })
        }
      },
      fail: function(res) {
        wx.showModal({
          title: '哎呀～',
          content: '连接失败了',
          success: function(res) {
            if (res.confirm) {
              console.log('用户点击确定')
            } else if (res.cancel) {
              console.log('用户点击取消')
            }
          }
        })
        console.log("fail!", res)
      },
      complete: function(res) {

      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})
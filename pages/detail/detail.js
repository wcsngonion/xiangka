// pages/shop/detail/detail.js
var util = require('../../utils/util.js');

Page({

  /**
   * 页面的初始数据
   */
  data: {
    is_use_payment: '0',
    source: 0,
    id: 0,
    name: "jhh",
    price: 0,
    original: 0,
    detail: "kugiyyg",
    in_detail: {
      "id": "1",
      "type": "times_card",
      "name": "白银卡",
      "original_price": "100",
      "actual_price": "0.09",
      "times": "3",
      "description": "<p>随意打的一段话<\/p>",
      "photo_url": "http:\/\/joyball.applinzi.com\/Public\/API\/image\/vip_card.png",
      "valid_start_timestamp": "1517414400",
      "valid_end_timestamp": "1556640000",
      "is_fixed_valid_days": "0",
      "valid_days": "0",
      "shop_id": "1",
      "company_id": "1",
      "create_timestamp": "1521950326",
      "goods": [{
        "id": "1",
        "name": "A级护理",
        "type": "1",
        "brief_description": "超级棒",
        "unit_price": "0.88",
        "original_price": "1.01",
        "discount": "0",
        "img_url": "http:\/\/joyball.applinzi.com\/Public\/API\/image\/vip_card.png",
        "category_id": "2",
        "create_timestamp": "1521790402",
        "shop_id": "1",
        "company_id": "1"
      }, {
        "id": "2",
        "name": "T级护理",
        "type": "1",
        "brief_description": "超级好吃超市好吃",
        "unit_price": "0.33",
        "original_price": "100",
        "discount": "0",
        "img_url": "http:\/\/joyball.applinzi.com\/Public\/API\/image\/vip_card.png",
        "category_id": "1",
        "create_timestamp": "1521790433",
        "shop_id": "1",
        "company_id": "1"
      }]
    },
    windowHeight: getApp().globalData.windowHeight,
    windowWidth: getApp().globalData.windowWidth,
    detailHeight: 0,
    buttonHeight: 0,
    imgUrls: [],
    comfirm_setting: {
      /*
address:"北京市 海淀区 北京邮电大学"
brief_describe:"本店所有的商品照片为专业摄影师拍摄，后期精心修制及色彩调整，尽量与实际商品保持一致，但由于拍摄时用光、角度、显示器色彩偏差、个人对颜色的认知等方面的差异，导致实物可能会与照片存在一些色差，最终颜色以实际商品为准。请在购买前与我们客服充分沟通后做出慎重选择。色差问题将不被我们认可当退换货的理由！"
company_en_name:"bupt"
company_id:"1"
company_name:"bysharer"
latitude:"0"
location:""
login_type:"1"
longitude:"0"
minapp_qrcode_url:"http://mmbiz.qpic.cn/mmbiz_jpg/BSywHp1iasmwRrNH7FTHGRZckmKicZvWsPRukCVfvA3zHsvjluohh6ShuyVjMxDIA4jjFqZSvDIiayWib5Vehxia0Nw/0"
open_period:"周一-周五 8：00-22：00"
phone:"18811593392"
photo_url:""
shop_id:"1"
shop_name:"北邮游泳馆"
target_page:"1"
version:"2"
*/
    }
  },

  toInstruction: function(){
    wx.navigateTo({
      url: '../instruction/instruction?url=' + this.data.in_detail.description,
    })
  },

  load_images: function () {
    this.setData({
      imgUrls: new Array(5).fill("/images/taocan.jpg"),
    })
  },

  back: function () {
    wx.navigateBack({
      delta: 1,
    })
  },

  buy: function () {
    var that = this
    if (that.data.is_use_payment == '1') {
      getApp().globalData.du_choose = that.data.in_detail
      wx.navigateTo({
        url: '../order/order',
      })
    }
    else {
      wx.showModal({
        title: '提示',
        showCancel: false,
        content: '请联系管理员购买！',
        success: function (res) { }
      })
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options)

    var that = this

    that.setData({
      comfirm_setting: getApp().globalData.settings,
      is_use_payment: getApp().globalData.settings.is_use_payment,
    })

    //test
    var test = ""
    for (var i = 0; i < 100; i++) {
      test += options.detail
    }

    if (options.source == 0) {
      that.setData({
        detailHeight: that.data.windowHeight * 0.92,
      })
    } else if (options.source == 1) {
      that.setData({
        detailHeight: that.data.windowHeight,
      })
    }

    this.setData({
      id: options.id,
      name: options.name,
      price: options.price,
      original: options.original,
      detail: test,
      buttonHeight: that.data.windowHeight * 0.08,
      source: options.source
    })

    wx.request({
      url: getApp().globalData.server + "/API/Card/get_one_times_card",
      data: {
        company_id: getApp().globalData.settings.company_id,
        shop_id: getApp().globalData.settings.shop_id,
        card_id: that.data.id
      },
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function (res) {
        console.log(res)
        if (res.data.error_no == 0) {
          that.setData({
            in_detail: res.data.data
          })
        } else {
          wx.showModal({
            title: '哎呀～',
            content: '获取会员卡详情失败',
            success: function (res) {
              if (res.confirm) {
                console.log('用户点击确定')
              } else if (res.cancel) {
                console.log('用户点击取消')
              }
            }
          })
        }
      },
      fail: function (res) {
        wx.showModal({
          title: '哎呀～',
          content: '获取会员卡详情失败',
          success: function (res) {
            if (res.confirm) {
              console.log('用户点击确定')
            } else if (res.cancel) {
              console.log('用户点击取消')
            }
          }
        })
        console.log("fail!", res)
      },
      complete: function (res) {

      }
    })

    this.load_images()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    this.setData({
      is_use_payment: getApp().globalData.settings.is_use_payment,
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.setData({
      is_use_payment: getApp().globalData.settings.is_use_payment,
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return (util.share())
  }
})
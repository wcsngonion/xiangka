// pages/load/load.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    scene: "",
    strs: "",
    show: false,
  },

  //页面跳转函数
  routePage: function() {
    //console.log(this.data.strs[0])
    switch (this.data.strs[0]) {
      case '1':
        {
          wx.redirectTo({
            url: '../payment/payment?goods_id=' + this.data.strs[1]
          })
        }
        break;
      case 'shared':
        {
          wx.redirectTo({
            url: '../shared_show/shared_show?share_id=' + this.data.strs[1]
          })
        }
        break;
      default:
        {
          if (getApp().globalData.settings.version == "1") {
            wx.redirectTo({
              url: '../index/index'
            })
          } else if (getApp().globalData.settings.version == "2") {
            wx.redirectTo({
              url: '../index/index'
            })
          } else if (getApp().globalData.settings.version == "3") {
            wx.redirectTo({
              url: '../another_index/another_index'
            })
          }

        }
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    //console.log(options)

    var that = this

    if (options != '' && options != undefined) {
      if (options.scene != '' && options.scene != undefined) {
        // options 中的 scene 需要使用 decodeURIComponent 才能获取到生成二维码时传入的 scene
        var scene = decodeURIComponent(options.scene)
        console.log('二维码scene：', this.data.scene)
        var strs = new Array(); //定义一数组
        strs = scene.split("/"); //字符分割
        this.data.strs = strs
      } else if (options.share_user != '' && options.share_user != undefined) {
        console.log("邀请分享参数", options)
        this.data.strs = ['shared', options.share_user]
      }
      console.log('路由参数数组', this.data.strs)
    }

    wx.showToast({
      title: '加载中',
      icon: 'loading',
      duration: 3600000,
      mask: true
    })

    wx.getExtConfig({
      success: function(res) {
        console.log('小程序模版配置', res.extConfig)
        if (res.extConfig.company_en_name != '' && res.extConfig.company_en_name != undefined) {
          getApp().globalData.company_en_name = res.extConfig.company_en_name
        }

        //before_check
        console.log("company_en_name:", getApp().globalData.company_en_name)

        //获取小程序设置
        wx.request({
          url: getApp().globalData.server + '/API/Index/get_system_info_by_english_name',
          data: {
            company_en_name: getApp().globalData.company_en_name,
          },
          method: "POST",
          header: {
            "Content-Type": "application/x-www-form-urlencoded"
          },
          success: function(res) {
            console.log('获取小程序设置', res.data)
            if (res.data.error_no != 0) {
              wx.showModal({
                title: '哎呀～',
                content: '出错了呢！' + res.data.data.error_msg,
                success: function(res) {
                  if (res.confirm) {
                    console.log('用户点击确定')
                  } else if (res.cancel) {
                    console.log('用户点击取消')
                  }
                }
              })
            } else if (res.data.error_no == 0) {
              getApp().globalData.settings = res.data.data

              //after_check
              //console.log(getApp().globalData.settings)

              wx.login({
                fail: function(res) {
                  console.log("fail", res)
                },
                complete: function(res) {
                  console.log("complete", res)
                },
                success: function(res) {
                  console.log("code:", res.code)
                  if (res.code) {

                    //发起网络请求

                    //before_check
                    // console.log("shop_id:", getApp().globalData.settings.shop_id)
                    // console.log("company_id:", getApp().globalData.settings.company_id)

                    //获取open_id和union_id
                    wx.request({
                      url: getApp().globalData.server + '/API/Login/get_user_info',
                      data: {
                        code: res.code,
                        shop_id: getApp().globalData.settings.shop_id,
                        company_id: getApp().globalData.settings.company_id,
                      },
                      method: "POST",
                      header: {
                        "Content-Type": "application/x-www-form-urlencoded"
                      },
                      success: function(res) {
                        console.log('获取open_id和union_id', res.data)
                        getApp().globalData.open_id = res.data.data.open_id
                        getApp().globalData.union_id = res.data.data.union_id

                        //after_check
                        // console.log('open_id:', getApp().globalData.open_id)
                        // console.log('union_id', getApp().globalData.union_id)

                        if (res.data.error_no != 0) {
                          wx.showModal({
                            title: '哎呀～',
                            content: '出错了呢！' + res.data.data.error_msg,
                            success: function(res) {
                              if (res.confirm) {
                                console.log('用户点击确定')
                              } else if (res.cancel) {
                                console.log('用户点击取消')
                              }
                            }
                          })
                        } else if (res.data.error_no == 0) {
                          // 查看是否授权
                          wx.getSetting({
                            success: function(res) {
                              if (res.authSetting['scope.userInfo']) {
                                // 已经授权，可以直接调用 getUserInfo 获取头像昵称
                                wx.getUserInfo({
                                  success: function(res) {
                                    //console.log(res.userInfo)
                                    getApp().globalData.userInfo = res.userInfo
                                    that.continuego()
                                  }
                                })
                              } else {
                                that.setData({
                                  show: true,
                                })
                                wx.hideToast()
                              }
                            },
                            fail: (res) => {
                              wx.showModal({
                                title: '哎呀～',
                                content: '获取设置出错了呢！请重新进入小程序~',
                                showCancel: '',
                                success: function(res) {
                                  if (res.confirm) {
                                    console.log('用户点击确定')
                                  } else if (res.cancel) {
                                    console.log('用户点击取消')
                                  }
                                }
                              })
                            },
                          })
                        }
                      },
                      fail: function(res) {
                        wx.showModal({
                          title: '哎呀～',
                          content: '网络不在状态呢！',
                          success: function(res) {
                            if (res.confirm) {
                              console.log('用户点击确定')
                            } else if (res.cancel) {
                              console.log('用户点击取消')
                            }
                          }
                        })
                      }
                    })
                  } else {
                    console.log('登录失败！' + res.errMsg)
                    wx.showModal({
                      title: '哎呀～',
                      content: '获取用户登录态失败！',
                      success: function(res) {
                        if (res.confirm) {
                          console.log('用户点击确定')
                        } else if (res.cancel) {
                          console.log('用户点击取消')
                        }
                      }
                    })
                  }
                },
              });
            }
          },
          fail: function(res) {
            wx.showModal({
              title: '哎呀～',
              content: '网络不在状态呢！',
              success: function(res) {
                if (res.confirm) {
                  console.log('用户点击确定')
                } else if (res.cancel) {
                  console.log('用户点击取消')
                }
              }
            })
          }
        })
      }
    })
  },

  bindGetUserInfo: function(e) {
    //console.log(e.detail.userInfo)

    var that = this

    wx.login({
      fail: function(res) {
        console.log("fail", res)
      },
      complete: function(res) {
        console.log("complete", res)
      },
      success: function(res) {
        console.log("code:", res.code)
        if (res.code) {
          wx.getUserInfo({
            success: function(res) {
              //console.log(res.userInfo)
              getApp().globalData.userInfo = res.userInfo
              that.continuego()
            },
            fail: function(res) {
              wx.showModal({
                title: '提示',
                content: '本程序需要授权才可使用，请您点击确定或进入用户设置页面开启授权~',
                success: function(res) {
                  if (res.confirm) {
                    console.log('用户点击确定')
                    wx.openSetting({
                      success: (res) => {
                        //console.log(res.authSetting)
                        if (res.authSetting["scope.userInfo"] == true) {
                          wx.redirectTo({
                            url: '../load/load'
                          })
                        }
                      }
                    })
                  } else if (res.cancel) {
                    console.log('用户点击取消')
                  }
                }
              })
            }
          })
        } else {
          console.log('登录失败！' + res.errMsg)
          wx.showModal({
            title: '哎呀～',
            content: '获取用户登录态失败！',
            success: function(res) {
              if (res.confirm) {
                console.log('用户点击确定')
              } else if (res.cancel) {
                console.log('用户点击取消')
              }
            }
          })
        }
      },
    });
  },

  continuego: function() {
    var that = this

    //check
    console.log('userInfo', getApp().globalData.userInfo)

    //检测注册
    wx.request({
      url: getApp().globalData.server + '/API/Login/is_register',
      data: {
        shop_id: getApp().globalData.settings.shop_id,
        company_id: getApp().globalData.settings.company_id,
        open_id: getApp().globalData.open_id,
        union_id: getApp().globalData.union_id,
        nickname: getApp().globalData.userInfo.nickName,
        headimgurl: getApp().globalData.userInfo.avatarUrl,
        sex: getApp().globalData.userInfo.gender,
      },
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function(res) {
        console.log('检测注册', res.data)
        if (res.data.error_no != 0) {
          wx.showModal({
            title: '哎呀～',
            content: '出错了呢！' + res.data.data.error_msg,
            success: function(res) {
              if (res.confirm) {
                console.log('用户点击确定')
              } else if (res.cancel) {
                console.log('用户点击取消')
              }
            }
          })
        } else if (res.data.error_no == 0) {
          getApp().globalData.userInfo_detail = res.data.data
          getApp().globalData.settings.brief_describe = res.data.data.shop.brief_describe
          getApp().globalData.settings.latitude = res.data.data.shop.latitude
          getApp().globalData.settings.location = res.data.data.shop.location
          getApp().globalData.settings.longitude = res.data.data.shop.longitude
          getApp().globalData.settings.open_period = res.data.data.shop.open_period
          getApp().globalData.settings.phone = res.data.data.shop.phone
          getApp().globalData.settings.photo_url = res.data.data.shop.photo_url
          getApp().globalData.settings.shop_id = res.data.data.shop.shop_id
          getApp().globalData.settings.shop_name = res.data.data.shop.shop_name
          //console.log(getApp().globalData.userInfo_detail)
          that.routePage()
        }
      },
      fail: function(res) {
        wx.showModal({
          title: '哎呀～',
          content: '网络不在状态呢！',
          success: function(res) {
            if (res.confirm) {
              console.log('用户点击确定')
            } else if (res.cancel) {
              console.log('用户点击取消')
            }
          }
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  // onShareAppMessage: function () {

  // }
})
// pages/my_list/my_list.js
var util = require('../../utils/util.js');
var timestamp_to_minite = util.timestamp_to_minite
var timestamp_to_day = util.timestamp_to_day

Page({

  /**
   * 页面的初始数据
   */
  data: {
    coupon_text: "折扣券",
    pay: 1,
    currentTab:0,
    winHeight: 0,
    winWidth: 0,
    showLoading: false,
    things_list: [],
    coupons_list:[],
    things_photo: [],
    switch_tab: [],
    vouchers_list: [],
    windowHeight: getApp().globalData.windowHeight,
    windowWidth: getApp().globalData.windowWidth,
    now_time:"0"
  },

  back: function () {
    wx.reLaunch({
      url: '../index/index',
    })
  },

  choose: function (e) {
    var thing = e.currentTarget.dataset.thing
    console.log(e.currentTarget)
    //getApp().globalData.du_choose = thing

    wx.navigateTo({
      url: '../detail/detail?name=' + thing.card_name + '&price=' + thing.actual_price + '&original=' + thing.original_price + '&id=' + thing.card_id + '&source=' + 1
    })
  },

  // call: function () {
  //   var that = this
  //   wx.makePhoneCall({
  //     phoneNumber: that.data.phone //仅为示例，并非真实的电话号码
  //   })
  // },


  get_info: function (e) {//折扣券的详细信息
    var that = this;
    const idx = e.currentTarget.dataset.idx;

    var switch_tab_temp = that.data.switch_tab;
    if (switch_tab_temp[idx] == 1) {
      switch_tab_temp[idx] = 0
    } else switch_tab_temp[idx] = 1;
    that.setData({
      switch_tab: switch_tab_temp,
    })
  },

  test: function () {
    this.setData({
      //things_list: [getApp().globalData.du_choose],
    })
  },
  use_discount: function () {
    wx.navigateTo({
      url: '../paying/paying',
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this
    var timestamp = Date.parse(new Date());
    timestamp = timestamp / 1000;
    timestamp = timestamp_to_day(parseInt(timestamp))
    

    that.setData({
      pay: options.pay,
      now_time: timestamp
    })
    /** 
     * 获取系统信息 
     */
    wx.getSystemInfo({
      success: function (res) {
        that.setData({
          winWidth: res.windowWidth,
          winHeight: res.windowHeight
        });
      }
    });

     //获得所有折扣券信息
      wx.request({
        url: getApp().globalData.server + "/API/Cash/get_all_cash_by_user_id",
        data: {
          company_id: getApp().globalData.settings.company_id,
          user_id: getApp().globalData.userInfo_detail.user_id,
          shop_id: getApp().globalData.settings.shop_id
        },
        method: "POST",
        header: {
          "Content-Type": "application/x-www-form-urlencoded"
        },
        success: function (res1) {
          if (res1.data.error_no == 0) {
            console.log("折扣券信息", res1)
            var vouchers_list_temp = res1.data.data
            // for (var j = 0; j < res1.data.data.length; j++) {
            for (var key in vouchers_list_temp) {
              vouchers_list_temp[key].valid_start_timestamp = timestamp_to_day(parseInt(vouchers_list_temp[key].valid_start_timestamp))
              vouchers_list_temp[key].valid_end_timestamp = timestamp_to_day(parseInt(vouchers_list_temp[key].valid_end_timestamp))
              
            }
            that.setData({

              vouchers_list: vouchers_list_temp,

            })
          } else {
            wx.showModal({
              title: '哎呀～',
              content: '获取优惠券列表失败',
              success: function (res1) {
                if (res1.confirm) {
                  console.log('用户点击确定')
                } else if (res1.cancel) {
                  console.log('用户点击取消')
                }
              }
            })
          }
        },
        fail: function (res) {
          wx.showModal({
            title: '哎呀～',
            content: '获取优惠券列表失败',
            success: function (res) {
              if (res.confirm) {
                console.log('用户点击确定')
              } else if (res.cancel) {
                console.log('用户点击取消')
              }
            }
          })
          console.log("fail!", res)
        },
        complete: function (res) {

        }
      })
    
    // this.test()
    //根据user_id获得指定用户的所有卡
    wx.request({
      url: getApp().globalData.server + '/API/Card/get_one_user_all_cards_by_user_id',
      data: {
        shop_id: getApp().globalData.settings.shop_id,
        company_id: getApp().globalData.settings.company_id,
        user_id: getApp().globalData.userInfo_detail.id,
        is_need_return_times_cards: 1,
        is_need_return_discount_cards: 1,
      },
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function (res1) {
        console.log(res1)
        if (res1.data.error_no != 0) {
          wx.showModal({
            title: '哎呀～',
            content: '获取卡券信息失败!',
            success: function (res) {
              if (res.confirm) {
                console.log('用户点击确定')
              } else if (res.cancel) {
                console.log('用户点击取消')
              }
            }
          })
        }
        else if (res1.data.error_no == 0) {
          console.log(res1)
          var things_list = res1.data.data.times_card.has_times_cards
          // var coupons_list = res1.data.data.discount_cards.has_discount_cards
          var coupon_list_temp = res1.data.data.discount_cards.has_discount_cards
          // for (var j = 0; j < res1.data.data.discount_cards.has_discount_cards.length; j++) {
          for (var key in coupon_list_temp) {
            coupon_list_temp[key].valid_start_timestamp = timestamp_to_day(parseInt(coupon_list_temp[key].valid_start_timestamp))
            coupon_list_temp[key].valid_end_timestamp = timestamp_to_day(parseInt(coupon_list_temp[key].valid_end_timestamp))
            coupon_list_temp[key].used_timestamp = timestamp_to_day(parseInt(coupon_list_temp[key].used_timestamp))
            
          }
          for (var key in things_list) {
            things_list[key].create_timestamp = timestamp_to_minite(parseInt(things_list[key].create_timestamp))
            things_list[key].valid_start_timestamp = timestamp_to_minite(parseInt(things_list[key].valid_start_timestamp))
            things_list[key].valid_end_timestamp = timestamp_to_minite(parseInt(things_list[key].valid_end_timestamp))
          }

          // var temp = "things_list[" + i + "]"
          // that.setData({
          //   [temp]: things_list[i]
          // })
          that.setData({
            things_list: things_list,
            coupons_list: coupon_list_temp
          })
             
          // that.load_images(that.data.things_list.length)
        }
      },
      fail: function (res) {
        wx.showModal({
          title: '哎呀～',
          content: '获取卡券信息失败',
          success: function (res) {
            if (res.confirm) {
              console.log('用户点击确定')
            } else if (res.cancel) {
              console.log('用户点击取消')
            }
          }
        })
      }
    })
  

  },
  


  load_images: function (length) {
    var that = this
    var index = 0
    wx.request({
      url: getApp().globalData.server + "/API/Card/get_one_times_card",
      data: {
        company_id: that.data.things_list[index].company_id,
        shop_id: that.data.things_list[index].shop_id,
        card_id: that.data.things_list[index].card_id
      },
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function (res) {
        console.log(res)
        if (res.data.error_no == 0) {
          index++
          that.setData({
            in_detail: res.data.data
          })
        } else {
          wx.showModal({
            title: '哎呀～',
            content: '获取会员卡详情失败！',
            success: function (res) {
              if (res.confirm) {
                console.log('用户点击确定')
              } else if (res.cancel) {
                console.log('用户点击取消')
              }
            }
          })
        }
      },
      fail: function (res) {
        wx.showModal({
          title: '哎呀～',
          content: '获取会员卡详情失败',
          success: function (res) {
            if (res.confirm) {
              console.log('用户点击确定')
            } else if (res.cancel) {
              console.log('用户点击取消')
            }
          }
        })
        console.log("fail!", res)
      },
      complete: function (res) {

      }
    })
  }, 
  bindChange: function (e) {
    var that = this;
    that.setData({ currentTab: e.detail.current });
  },
  swichNav: function (e) {
    var that = this;
    if (this.data.currentTab === e.target.dataset.current) {
      return false;
    } else {
      that.setData({
        currentTab: e.target.dataset.current
      })}
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return (util.share())
  },

  second_select: function () {
    wx.scanCode({
      onlyFromCamera: true,
      success: (res) => {
        console.log(res)
      }
    })
  },

  third_select: function () {
    wx.redirectTo({
      url: '/pages/mine/mine'
    })
  }
})
// pages/index/index.js
var util = require('../../utils/util.js');
var timestamp_to_minite = require('../../utils/util.js').timestamp_to_day;

Page({

  /**
   * 页面的初始数据
   */
  data: {
    //滚动字幕--yi
    text: '',
    marqueePace: 0.8, //滚动速度
    marqueeDistance: 0, //初始滚动距离
    marqueeDistance2: 0,
    marquee2copy_status: false,
    marquee2_margin: 60,
    size: 14,
    orientation: 'left', //滚动方向
    interval2: 20, // 时间间隔
    //end--yi

    showLoading: false,
    coupon_text: "折扣券",
    things_list: [{
      "id": "1",
      "name": "白银卡",
      "original_price": "100",
      "actual_price": "0.09",
      "times": "3",
      "photo_url": "http:\/\/joyball.applinzi.com\/Public\/API\/image\/vip_card.png",
      "valid_start_timestamp": "1517414400",
      "valid_end_timestamp": "1556640000",
      "is_fixed_valid_days": "0",
      "valid_days": "0",
      "shop_id": "1",
      "company_id": "1",
      "applied_fields": " A级护理 T级护理"
    }],
    show_data: [],
    coupons_list: [{
      "id": "8",
      "name": "小皮筋",
      "original_price": "100",
      "actual_price": "50",
      "description": "",
      "photo_url": null,
      "discount": "0.65",
      "valid_start_timestamp": "1530720720",
      "valid_end_timestamp": "1531023120",
      "is_fixed_valid_days": "0",
      "valid_days": "0",
      "total_num": "100",
      "consumed_num": "0",
      "allow_be_got_start_timestamp": "1528190880",
      "allow_be_got_end_timestamp": "1528450260",
      "each_person_may_receive": "10",
      "is_show": "1",
      "at_least_consume_money": "20",
      "allow_be_used_times": "10",
      "must_at_least_cost_money": "10",
      "is_just_allow_new_customer": "1",
      "state": "1",
      "shop_id": "1",
      "company_id": "1",
      "create_timestamp": "1528024356",
      "user_id": null,
      "is_has_been_got": 0
    }],
    switch_tab: [],
    vouchers_list: [{
        "id": "7",
        "name": "小皮筋",
        "original_price": "100",
        "actual_price": "50",
        "description": "",
        "photo_url": null,
        "discount": "0.65",
        "valid_start_timestamp": "1530720720",
        "valid_end_timestamp": "1531023120",
        "is_fixed_valid_days": "0",
        "valid_days": "0",
        "total_num": "100",
        "consumed_num": "0",
        "allow_be_got_start_timestamp": "1528190880",
        "allow_be_got_end_timestamp": "1528450260",
        "each_person_may_receive": "10",
        "is_show": "1",
        "at_least_consume_money": "20",
        "allow_be_used_times": "10",
        "must_at_least_cost_money": "10",
        "is_just_allow_new_customer": "1",
        "state": "1",
        "shop_id": "1",
        "company_id": "1",
        "create_timestamp": "1528024356",
        "user_id": null,
        "is_has_been_got": 0
      },
      {
        "id": "8",
        "name": "小皮筋",
        "original_price": "100",
        "actual_price": "50",
        "description": "",
        "photo_url": null,
        "discount": "0.65",
        "valid_start_timestamp": "1530720720",
        "valid_end_timestamp": "1531023120",
        "is_fixed_valid_days": "0",
        "valid_days": "0",
        "total_num": "100",
        "consumed_num": "0",
        "allow_be_got_start_timestamp": "1528190880",
        "allow_be_got_end_timestamp": "1528450260",
        "each_person_may_receive": "10",
        "is_show": "1",
        "at_least_consume_money": "20",
        "allow_be_used_times": "10",
        "must_at_least_cost_money": "10",
        "is_just_allow_new_customer": "1",
        "state": "1",
        "shop_id": "1",
        "company_id": "1",
        "create_timestamp": "1528024356",
        "user_id": null,
        "is_has_been_got": 0
      }
    ],
    display_discount: 9,
    windowHeight: getApp().globalData.windowHeight,
    windowWidth: getApp().globalData.windowWidth,
    select_tab: 1,
    comfirm_setting: {
      /*
address:"北京市 海淀区 北京邮电大学"
brief_describe:"本店所有的商品照片为专业摄影师拍摄，后期精心修制及色彩调整，尽量与实际商品保持一致，但由于拍摄时用光、角度、显示器色彩偏差、个人对颜色的认知等方面的差异，导致实物可能会与照片存在一些色差，最终颜色以实际商品为准。请在购买前与我们客服充分沟通后做出慎重选择。色差问题将不被我们认可当退换货的理由！"
company_en_name:"bupt"
company_id:"1"
company_name:"bysharer"
latitude:"0"
location:""
login_type:"1"
longitude:"0"
minapp_qrcode_url:"http://mmbiz.qpic.cn/mmbiz_jpg/BSywHp1iasmwRrNH7FTHGRZckmKicZvWsPRukCVfvA3zHsvjluohh6ShuyVjMxDIA4jjFqZSvDIiayWib5Vehxia0Nw/0"
open_period:"周一-周五 8：00-22：00"
phone:"18811593392"
photo_url:""
shop_id:"1"
shop_name:"北邮游泳馆"
target_page:"1"
version:"2"
*/
    },

    // y's code start
    scene: "",
    strs: "",
    isshow: false,
    add_point: "0",
    current_point: "0",
    // y's code end

    has_been_got: [],
    voucher_has_been_got: [],
    remained_num: [],
    imgUrls: [{
      link: '/pages/index/index',
      url: ''
    }, {
      link: '/pages/index/index',
      url: ''
    }, {
      link: '/pages/index/index',
      url: ''
    }],
    indicatorDots: true,
    autoplay: true,
    interval: 5000,
    duration: 1000,
  },

  /* cbd页  start 杜思聪*/
  toAnotherIndex: function() {
    wx.navigateTo({
      url: '../another_index/another_index',
    })
  },

  /* cbd页  end 杜思聪*/

  /* cbd页  start y */
  changeshop: function() {
    wx.navigateTo({
      url: '../exshop/exshop',
    })
  },

  /* cbd页  end y */

  about: function() {
    wx.navigateTo({
      url: '../about/about'
    })
  },

  choose: function(e) {
    var thing = e.currentTarget.dataset.thing
    // console.log(e.currentTarget)
    //getApp().globalData.du_choose = thing

    wx.navigateTo({
      url: '../detail/detail?name=' + thing.name + '&price=' + thing.price + '&mark=' + thing.mark + '&detail=' + thing.detail + '&original=' + thing.original_price + '&id=' + thing.id + '&source=' + 0
    })
  },

  call: function() {
    var that = this
    wx.makePhoneCall({
      phoneNumber: that.data.comfirm_setting.phone //仅为示例，并非真实的电话号码
    })
  },

  test: function() {
    var example = {
      id: 4,
      photo: "/images/taocan.jpg",
      name: "钻石卡",
      price: 100.00,
      original_price: 120.00,
      detail: "详情"
    }

    this.setData({
      things_list: new Array(5).fill(example),
    })
  },

  scan: function() {
    var that = this
    wx.scanCode({
      onlyFromCamera: true,
      success: (res) => {
        console.log(res)
        var _scene = util.parseQueryString(res.path);
        console.log(_scene)
        var scene = decodeURIComponent(_scene.scene)
        this.data.scene = scene
        console.log('二维码scene：', this.data.scene)
        var strs = new Array(); //定义一数组
        strs = scene.split("/"); //字符分割
        this.data.strs = strs
        console.log('路由参数数组', this.data.strs)
        this.routePage()
      }
    })
  },

  get_info: function(e) {
    var that = this;
    const idx = e.currentTarget.dataset.idx;

    var switch_tab_temp = that.data.switch_tab;
    if (switch_tab_temp[idx] == 1) {
      switch_tab_temp[idx] = 0
    } else switch_tab_temp[idx] = 1;
    that.setData({
      switch_tab: switch_tab_temp,
    })
  },

  // y's code start
  formSubmit: function(e) {
    var that = this
    this.setData({
      isshow: false,
    })
    console.log('积分变动微信提醒(formId)：', e.detail.formId)
    //积分变动微信提醒
    wx.request({
      url: getApp().globalData.server + '/API/Point/point_wechat_notify',
      data: {
        company_id: getApp().globalData.settings.company_id,
        shop_id: getApp().globalData.settings.shop_id,
        user_id: getApp().globalData.userInfo_detail.user_id,
        username: getApp().globalData.userInfo_detail.username,
        openid: getApp().globalData.open_id,
        form_id: e.detail.formId,
        points_type: '新增',
        points_from: '当日首次登陆',
        consume_points: that.data.add_point,
        total_points: that.data.current_point,
      },
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function(res) {
        console.log('积分变动微信提醒', res.data)
        if (res.data.error_no != 0) {
          // wx.showModal({
          //   title: '哎呀～',
          //   content: '出错了呢！' + res.data.data.error_msg,
          //   success: function (res) {
          //     if (res.confirm) {
          //       console.log('用户点击确定')
          //     } else if (res.cancel) {
          //       console.log('用户点击取消')
          //     }
          //   }
          // })
        } else if (res.data.error_no == 0) {}
      },
      fail: function(res) {
        // wx.showModal({
        //   title: '哎呀～',
        //   content: '网络不在状态呢！',
        //   success: function (res) {
        //     if (res.confirm) {
        //       console.log('用户点击确定')
        //     } else if (res.cancel) {
        //       console.log('用户点击取消')
        //     }
        //   }
        // })
      }
    })
  },
  // y's code end

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    if (getApp().globalData.settings.is_on_time_pay == '1') {
      wx.setNavigationBarTitle({
        title: getApp().globalData.settings.company_name
      })
    }
    // this.test()
    var that = this
    that.setData({
      comfirm_setting: getApp().globalData.settings,
      text: getApp().globalData.settings.advertisement_text,
    })
    that.get_banners()
    that.get_cards_list()
    that.get_discount_list()
    that.get_all_cash()
    console.log(getApp().globalData)

    // y's code start
    //检查每日登录成功
    wx.request({
      url: getApp().globalData.server + '/API/Login/daily_login',
      data: {
        shop_id: getApp().globalData.settings.shop_id,
        company_id: getApp().globalData.settings.company_id,
        user_id: getApp().globalData.userInfo_detail.user_id,
      },
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function(res) {
        console.log('检查每日登录成功', res.data)
        if (res.data.error_no != 0) {
          if (res.data.error_no != 10086) {
            wx.showModal({
              title: '哎呀～',
              content: '出错了呢！' + res.data.data.error_msg,
              success: function(res) {
                if (res.confirm) {
                  console.log('用户点击确定')
                } else if (res.cancel) {
                  console.log('用户点击取消')
                }
              }
            })
          }
        } else if (res.data.error_no == 0) {
          getApp().globalData.userInfo_detail.point = res.data.data.current_point
          that.setData({
            isshow: true,
            add_point: res.data.data.add_point,
            current_point: res.data.data.current_point,
          })
        }
      },
      fail: function(res) {
        wx.showModal({
          title: '哎呀～',
          content: '网络不在状态呢！',
          success: function(res) {
            if (res.confirm) {
              console.log('用户点击确定')
            } else if (res.cancel) {
              console.log('用户点击取消')
            }
          }
        })
      }
    })
    // y's code end
  },

  get_cards_list: function() {
    var that = this
    wx.request({
      url: getApp().globalData.server + "/API/Card/get_shop_allcard",
      data: {
        company_id: getApp().globalData.settings.company_id,
        shop_id: getApp().globalData.settings.shop_id
      },
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function(res) {
        // console.log(res)
        if (res.data.error_no == 0) {
          that.setData({
            things_list: res.data.data.times_cards
          })
        } else {
          wx.showModal({
            title: '哎呀～',
            content: '获取套餐列表失败',
            success: function(res) {
              if (res.confirm) {
                console.log('用户点击确定')
              } else if (res.cancel) {
                console.log('用户点击取消')
              }
            }
          })
        }
      },
      fail: function(res) {
        wx.showModal({
          title: '哎呀～',
          content: '获取套餐列表失败',
          success: function(res) {
            if (res.confirm) {
              console.log('用户点击确定')
            } else if (res.cancel) {
              console.log('用户点击取消')
            }
          }
        })
        console.log("fail!", res)
      },
      complete: function(res) {

      }
    })
  },
  get_discount_list: function() { //获得所有优惠券信息
    var that = this
    wx.request({
      url: getApp().globalData.server + "/API/Card/get_all_discount_card",
      data: {
        company_id: getApp().globalData.settings.company_id,
        user_id: getApp().globalData.userInfo_detail.user_id,
        shop_id: getApp().globalData.settings.shop_id
      },
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function(res1) {
        console.log(res1)
        if (res1.data.error_no == 0) {

          var has_been_got_temp = []
          var remained_num = []
          for (var k = 0; k < res1.data.data.discount_cards.length; k++) {
            has_been_got_temp.push(res1.data.data.discount_cards[k].is_has_been_got)
            remained_num.push(res1.data.data.discount_cards[k].remained_num)
          }
          var coupon_list_temp = res1.data.data.discount_cards
          for (var j = 0; j < res1.data.data.discount_cards.length; j++) {
            if (coupon_list_temp[j].is_fixed_valid_days == "0") {
              coupon_list_temp[j].valid_start_timestamp = timestamp_to_minite(parseInt(coupon_list_temp[j].valid_start_timestamp))
              coupon_list_temp[j].valid_end_timestamp = timestamp_to_minite(parseInt(coupon_list_temp[j].valid_end_timestamp))
            }
          }

          that.setData({
            // coupons_list: res1.data.data.discount_cards,
            coupons_list: coupon_list_temp,
            has_been_got: has_been_got_temp,
            remained_num: remained_num
          })
        } else {
          wx.showModal({
            title: '哎呀～',
            content: '获取优惠券列表失败',
            success: function(res1) {
              if (res1.confirm) {
                console.log('用户点击确定')
              } else if (res1.cancel) {
                console.log('用户点击取消')
              }
            }
          })
        }
      },
      fail: function(res) {
        wx.showModal({
          title: '哎呀～',
          content: '获取优惠券列表失败',
          success: function(res) {
            if (res.confirm) {
              console.log('用户点击确定')
            } else if (res.cancel) {
              console.log('用户点击取消')
            }
          }
        })
        console.log("fail!", res)
      },
      complete: function(res) {

      }
    })
  },

  get_all_cash: function() { //获得所有代金券信息
    var that = this
    wx.request({
      url: getApp().globalData.server + "/API/Cash/get_all_cash",
      data: {
        company_id: getApp().globalData.settings.company_id,
        user_id: getApp().globalData.userInfo_detail.user_id,
        shop_id: getApp().globalData.settings.shop_id
      },
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function(res1) {
        if (res1.data.error_no == 0) {
          console.log("折扣券信息", res1)
          var has_been_got_temp = []
          for (var k = 0; k < res1.data.data.cashes.length; k++) {
            has_been_got_temp.push(res1.data.data.cashes[k].is_has_been_got)
          }
          var vouchers_list_temp = res1.data.data.cashes
          for (var j = 0; j < res1.data.data.cashes.length; j++) {
            if (vouchers_list_temp[j].is_fixed_valid_days == "0") {
              vouchers_list_temp[j].valid_start_timestamp = timestamp_to_minite(parseInt(vouchers_list_temp[j].valid_start_timestamp))
              vouchers_list_temp[j].valid_end_timestamp = timestamp_to_minite(parseInt(vouchers_list_temp[j].valid_end_timestamp))
            }
          }
          console.log(has_been_got_temp)
          that.setData({
            voucher_has_been_got: has_been_got_temp,
            vouchers_list: vouchers_list_temp,

          })
        } else {
          wx.showModal({
            title: '哎呀～',
            content: '获取优惠券列表失败',
            success: function(res1) {
              if (res1.confirm) {
                console.log('用户点击确定')
              } else if (res1.cancel) {
                console.log('用户点击取消')
              }
            }
          })
        }
      },
      fail: function(res) {
        wx.showModal({
          title: '哎呀～',
          content: '获取优惠券列表失败',
          success: function(res) {
            if (res.confirm) {
              console.log('用户点击确定')
            } else if (res.cancel) {
              console.log('用户点击取消')
            }
          }
        })
        console.log("fail!", res)
      },
      complete: function(res) {

      }
    })
  },


  get_discount: function(e) { //获得优惠券
    var that = this;
    const idx = e.currentTarget.dataset.idx;

    var index = e.currentTarget.dataset.index
    var has_been_got_temp = that.data.has_been_got
    var remained_num = that.data.remained_num
    has_been_got_temp[index] = 1
    remained_num[index] = remained_num[index] - 1
    // console.log(111,idx);
    wx.request({
      url: getApp().globalData.server + "/API/Card/receive_discount_card",
      data: {
        company_id: getApp().globalData.settings.company_id,
        user_id: getApp().globalData.userInfo_detail.user_id,
        shop_id: getApp().globalData.settings.shop_id,
        username: getApp().globalData.userInfo_detail.username,
        card_id: idx
      },
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function(res1) {
        console.log(res1)
        if (res1.data.error_no == 0) {
          that.setData({
            has_been_got: has_been_got_temp,
            remained_num: remained_num
          })
          wx.showModal({
            title: '领取成功',
            content: '可以在在线买单中使用哦~',
            success: function(res2) {
              if (res2.confirm) {
                console.log('用户点击确定')
              } else if (res2.cancel) {
                console.log('用户点击取消')
              }
            }
          })
          // console.log(res1.data.data.discount_cards.discount)
        } else {
          wx.showModal({
            title: '哎呀～',
            content: '领取失败',
            success: function(res1) {
              if (res1.confirm) {
                console.log('用户点击确定')
              } else if (res1.cancel) {
                console.log('用户点击取消')
              }
            }
          })
        }
      },
      fail: function(res) {
        wx.showModal({
          title: '哎呀～',
          content: '获取优惠券列表失败',
          success: function(res) {
            if (res.confirm) {
              console.log('用户点击确定')
            } else if (res.cancel) {
              console.log('用户点击取消')
            }
          }
        })
        console.log("fail!", res)
      },
      complete: function(res) {

      }
    })
    that.get_discount_list()
  },

  receive_cash: function(e) { //点击获得代金券
    var that = this;
    const voucherid = e.currentTarget.dataset.voucherid;
    console.log(voucherid)
    var index = e.currentTarget.dataset.index
    var has_been_got_temp = that.data.voucher_has_been_got
    has_been_got_temp[index] = 1
    // remained_num[index] = remained_num[index] - 1
    // console.log(111,idx);
    wx.request({
      url: getApp().globalData.server + "/API/Cash/receive_cash",
      data: {
        company_id: getApp().globalData.settings.company_id,
        user_id: getApp().globalData.userInfo_detail.user_id,
        shop_id: getApp().globalData.settings.shop_id,
        username: getApp().globalData.userInfo_detail.username,
        voucher_id: voucherid
      },
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function(res1) {
        console.log(res1)
        if (res1.data.error_no == 0) {
          that.setData({
            voucher_has_been_got: has_been_got_temp,
          })
          wx.showModal({
            title: '领取成功',
            content: '可以在在线买单中使用哦~',
            success: function(res2) {
              if (res2.confirm) {
                console.log('用户点击确定')
              } else if (res2.cancel) {
                console.log('用户点击取消')
              }
            }
          })
          // console.log(res1.data.data.discount_cards.discount)
        } else {
          wx.showModal({
            title: '哎呀～',
            content: '领取失败',
            success: function(res1) {
              if (res1.confirm) {
                console.log('用户点击确定')
              } else if (res1.cancel) {
                console.log('用户点击取消')
              }
            }
          })
        }
      },
      fail: function(res) {
        wx.showModal({
          title: '哎呀～',
          content: '获取折扣券列表失败',
          success: function(res) {
            if (res.confirm) {
              console.log('用户点击确定')
            } else if (res.cancel) {
              console.log('用户点击取消')
            }
          }
        })
        console.log("fail!", res)
      },
      complete: function(res) {

      }
    })
    that.get_discount_list()
  },

  get_banners: function(e) { //轮播图
    var that = this
    wx.request({
      url: getApp().globalData.server + "/API/Index/get_banners",
      data: {
        company_id: getApp().globalData.settings.company_id,
        shop_id: getApp().globalData.settings.shop_id,
      },
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function(res1) {
        console.log(res1)
        if (res1.data.error_no == 0) {
          console.log(res1)
          that.setData({
            imgUrls: res1.data.data
          })
        } else {
          wx.showModal({
            title: '哎呀～',
            content: '连接失败',
            success: function(res1) {
              if (res1.confirm) {
                console.log('用户点击确定')
              } else if (res1.cancel) {
                console.log('用户点击取消')
              }
            }
          })
        }
      },
      fail: function(res) {
        wx.showModal({
          title: '哎呀～',
          content: '连接失败了',
          success: function(res) {
            if (res.confirm) {
              console.log('用户点击确定')
            } else if (res.cancel) {
              console.log('用户点击取消')
            }
          }
        })
        console.log("fail!", res)
      },
      complete: function(res) {

      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {
    this.setData({
      text: getApp().globalData.settings.advertisement_text,
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    var tmp = 'comfirm_setting.shop_name'
    this.setData({
      [tmp]: getApp().globalData.settings.shop_name,
      comfirm_setting: getApp().globalData.settings,
      text: getApp().globalData.settings.advertisement_text,
    })
    // 页面显示
    var vm = this;
    var length = vm.data.text.length * vm.data.size; //文字长度
    var windowWidth = wx.getSystemInfoSync().windowWidth; // 屏幕宽度
    vm.setData({
      length: length,
      windowWidth: windowWidth,
      marquee2_margin: length < windowWidth ? windowWidth - length : vm.data.marquee2_margin //当文字长度小于屏幕长度时，需要增加补白
    });
    vm.run(); // 第一个字消失后立即从右边出现
  },

  run: function() {
    var vm = this;
    var interval = setInterval(function() {
      if (-vm.data.marqueeDistance2 < vm.data.length) {
        // 如果文字滚动到出现marquee2_margin=30px的白边，就接着显示
        vm.setData({
          marqueeDistance2: vm.data.marqueeDistance2 - vm.data.marqueePace,
          marquee2copy_status: vm.data.length + vm.data.marqueeDistance2 <= vm.data.windowWidth + vm.data.marquee2_margin,
        });
      } else {
        if (-vm.data.marqueeDistance2 >= vm.data.marquee2_margin) { // 当第二条文字滚动到最左边时
          vm.setData({
            marqueeDistance2: vm.data.marquee2_margin // 直接重新滚动
          });
          clearInterval(interval);
          vm.run();
        } else {
          clearInterval(interval);
          vm.setData({
            marqueeDistance2: -vm.data.windowWidth
          });
          vm.run();
        }
      }
    }, vm.data.interval2);
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {
    this.get_cards_list()
    this.get_discount_list()
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {
    return (util.share())
  },

  second_select: function() {
    wx.redirectTo({
      url: '/pages/service/service'
    })
  },

  routePage: function() {
    if (this.data.scene == '' || this.data.scene == undefined) {
      wx.redirectTo({
        url: '../index/index'
      })
    } else {
      //console.log(this.data.strs[0])
      switch (this.data.strs[0]) {
        case '1':
          {
            wx.redirectTo({
              url: '../payment/payment?goods_id=' + this.data.strs[1]
            })
          }
          break;
        default:
          {
            wx.redirectTo({
              url: '../index/index'
            })
          }
      }
    }
  },

  third_select: function() {
    wx.redirectTo({
      url: '/pages/mine/mine'
    })
  },
  daka: function() {
    wx.navigateTo({
      url: '../clock/clock'
    })
  },
  toPayment: function() {
    wx.navigateTo({
      url: '../payment/payment',
    })
  },
  toPaying: function() {
    wx.navigateTo({
      url: '../paying/paying',
    })
  },
  recha: function() {
    wx.navigateTo({
      url: '../recharge/recharge'
    })
  }
})
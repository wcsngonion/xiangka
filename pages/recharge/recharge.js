// pages/recharge/recharge.js
var util = require('../../utils/util.js');

Page({

  /**
   * 页面的初始数据
   */
  data: {
    username: "会员卡",//用户名称
    userphoto: getApp().globalData.userInfo.avatarUrl,//用户头像
    mbalance: "0.00",//主账户余额
    sbalance: "0.00",//副账户余额
    rechargeitems: [],
    choosed: [],
    is_use_payment: '0',

    //充值过程
    total_to_pay_money: 0,
    order_id: '',
  },

  choose: function (e) {
    //console.log(e)
    const idx = e.currentTarget.dataset.idx;
    this.setData({
      choosed: this.data.rechargeitems[idx],
    });
  },

  click: function () {
    var that = this
    if (that.data.is_use_payment == '1') {
      if (that.data.choosed == '') {
        wx.showModal({
          title: '提示',
          showCancel: false,
          content: '请选择一种套餐！',
          success: function (res) { }
        })
      }
      else {
        //伪造的商品数据
        var goods_arr_tmp = [
          {
            goods_type: 3,
            goods_id: that.data.choosed.id,
            goods_name: '充' + that.data.choosed.min_level_money + '送' + that.data.choosed.present_money,
            unit_price: that.data.choosed.min_level_money,
            original_price: that.data.choosed.min_level_money,
            discount: 0,
            quantity: 1,
            note: "微信储值",
            company_id: getApp().globalData.settings.company_id,
            shop_id: getApp().globalData.settings.shop_id
          }
        ]
        console.log("goods_arr", goods_arr_tmp)
        //创建订单，获得待支付总金额
        wx.request({
          url: getApp().globalData.server + '/API/Shopping/do_order',
          data: {
            shop_id: getApp().globalData.settings.shop_id,
            company_id: getApp().globalData.settings.company_id,
            user_id: getApp().globalData.userInfo_detail.user_id,
            username: getApp().globalData.userInfo_detail.username,
            note: "微信储值",
            order_type: 3,
            goods_arr: JSON.stringify(goods_arr_tmp),
          },
          method: "POST",
          header: {
            "Content-Type": "application/x-www-form-urlencoded"
          },
          success: function (res) {
            console.log('创建订单，获得待支付总金额', res.data)
            if (res.data.error_no != 0) {
              wx.showModal({
                title: '哎呀～',
                content: '出错了呢！' + res.data.data.error_msg,
                success: function (res) {
                  if (res.confirm) {
                    console.log('用户点击确定')
                  } else if (res.cancel) {
                    console.log('用户点击取消')
                  }
                }
              })
            }
            else if (res.data.error_no == 0) {
              that.data.total_to_pay_money = res.data.data.total_to_pay_money
              console.log('需支付金额（total_to_pay_money）：', that.data.total_to_pay_money)
              that.data.order_id = res.data.data.order_id
              console.log('订单号（order_id）：', that.data.order_id)
              //微信支付
              wx.login({
                success: function (res) {
                  if (res.code) {
                    console.log('code', res.code, "user_id", getApp().globalData.userInfo_detail.user_id)
                    wx.request({
                      url: getApp().globalData.server + "/API/Pay/pay_request",
                      data: {
                        code: res.code,
                        company_id: getApp().globalData.settings.company_id,
                        shop_id: getApp().globalData.settings.shop_id,
                        order_id: that.data.order_id,
                        total_to_pay_money: that.data.total_to_pay_money,
                        user_id: getApp().globalData.userInfo_detail.user_id
                      },
                      method: "POST",
                      header: {
                        "Content-Type": "application/x-www-form-urlencoded"
                      },
                      success: function (res2) {
                        console.log(res2)
                        if (res2.data.error_no != 0) {
                          wx.showModal({
                            title: '哎呀',
                            content: '请求支付失败！',
                            success: function (res) {
                              if (res.confirm) {
                                console.log('用户点击确定')
                              } else if (res.cancel) {
                                console.log('用户点击取消')
                              }
                            }
                          })
                        }
                        else if (res2.data.error_no == 0) {
                          wx.requestPayment(
                            {
                              'timeStamp': res2.data.data.timeStamp + '',
                              'nonceStr': res2.data.data.nonceStr,
                              'package': res2.data.data.package,
                              'signType': 'MD5',
                              'paySign': res2.data.data.paySign,
                              'success': function (res) {
                                wx.showModal({
                                  title: '恭喜',
                                  showCancel: false,
                                  content: '支付成功！',
                                  success: function (res) {
                                    wx.reLaunch({
                                      url: '../load/load'
                                    })
                                  }
                                })
                              },
                              'fail': function (res) {
                                wx.showModal({
                                  title: '提示',
                                  showCancel: false,
                                  content: '支付失败，请刷新后重试！',
                                  success: function (res) { }
                                })
                              },
                              'complete': function (res) { }
                            }
                          )
                        }
                      },
                      fail: function (res) {
                        wx.showModal({
                          title: '哎呀～',
                          content: '请求支付失败！',
                          success: function (res) {
                            if (res.confirm) {
                              console.log('用户点击确定')
                            } else if (res.cancel) {
                              console.log('用户点击取消')
                            }
                          }
                        })
                      }
                    })
                  } else {
                    console.log('登录失败！' + res.errMsg)
                    wx.showModal({
                      title: '哎呀～',
                      content: '内部错误1',
                      success: function (res) {
                        if (res.confirm) {
                          console.log('用户点击确定')
                        } else if (res.cancel) {
                          console.log('用户点击取消')
                        }
                      }
                    })
                  }
                }
              });
            }
          },
          fail: function (res) {
            wx.showModal({
              title: '哎呀～',
              content: '网络不在状态呢！',
              success: function (res) {
                if (res.confirm) {
                  console.log('用户点击确定')
                } else if (res.cancel) {
                  console.log('用户点击取消')
                }
              }
            })
          }
        })
      }
    }
    else {
      wx.showModal({
        title: '提示',
        showCancel: false,
        content: '请联系管理员充值！',
        success: function (res) { }
      })
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this
    //获取公司的所有充值规则
    wx.request({
      url: getApp().globalData.server + '/API/Deposit/get_deposit_rule_by_company',
      data: {
        shop_id: getApp().globalData.settings.shop_id,
        company_id: getApp().globalData.settings.company_id,
      },
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function (res) {
        console.log('获取公司的所有充值规则', res.data)
        if (res.data.error_no != 0) {
          wx.showModal({
            title: '哎呀～',
            content: '出错了呢！' + res.data.data.error_msg,
            success: function (res) {
              if (res.confirm) {
                console.log('用户点击确定')
              } else if (res.cancel) {
                console.log('用户点击取消')
              }
            }
          })
        }
        else if (res.data.error_no == 0) {
          that.setData({
            rechargeitems: res.data.data.all_rules
          })
        }
      },
      fail: function (res) {
        wx.showModal({
          title: '哎呀～',
          content: '网络不在状态呢！',
          success: function (res) {
            if (res.confirm) {
              console.log('用户点击确定')
            } else if (res.cancel) {
              console.log('用户点击取消')
            }
          }
        })
      }
    })
    that.setData({
      userphoto: getApp().globalData.userInfo.avatarUrl,
      is_use_payment: getApp().globalData.settings.is_use_payment,
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    this.setData({
      userphoto: getApp().globalData.userInfo.avatarUrl,
      is_use_payment: getApp().globalData.settings.is_use_payment,
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.setData({
      is_use_payment: getApp().globalData.settings.is_use_payment,
    })
    if (getApp().globalData.userInfo != "") {
      this.setData({
        userphoto: getApp().globalData.userInfo.avatarUrl
      })
    }
    else {
      this.setData({
        userphoto: "/images/pho.png",
      })
    }
    if (getApp().globalData.userInfo_detail.is_phone_login == 1) {
      this.setData({
        username: getApp().globalData.userInfo_detail.username,
      })
    }
    else {
      this.setData({
        username: getApp().globalData.userInfo.nickName,
      })
    }
    if (getApp().globalData.userInfo_detail.main_balance != "" && getApp().globalData.userInfo_detail.second_balance != "") {
      var main = parseFloat(getApp().globalData.userInfo_detail.main_balance)
      var second = parseFloat(getApp().globalData.userInfo_detail.second_balance)
      //
      var main_value = Math.round(parseFloat(main) * 100) / 100
      var second_value = Math.round(parseFloat(second) * 100) / 100
      //
      var main_xsd = main_value.toString().split(".")
      var second_xsd = second_value.toString().split(".")
      //
      if (main_xsd.length == 1) {
        main_value = main_value.toString() + ".00";
        this.setData({
          mbalance: main_value,
        })
      }
      if (main_xsd.length > 1) {
        if (main_xsd[1].length < 2) {
          main_xsd = main_value.toString() + "0";
        }
        this.setData({
          mbalance: main_value,
        })
      }
      //
      if (second_xsd.length == 1) {
        second_value = second_value.toString() + ".00";
        this.setData({
          sbalance: second_value,
        })
      }
      if (second_xsd.length > 1) {
        if (second_xsd[1].length < 2) {
          second_xsd = second_value.toString() + "0";
        }
        this.setData({
          sbalance: second_value,
        })
      }
      //console.log(main_value, second_value)
    }
    if (getApp().globalData.userInfo_detail.is_phone_login == 1) {
      this.setData({
        is_phone_login: true
      })
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return (util.share())
  }
})
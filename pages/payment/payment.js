// pages/payment/payment.js
var util = require('../../utils/util.js');
var app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    shop_name: 'Getting Data...',
    goods_id: '',
    is_use_payment: '0',

    //滚动条相关设置
    text: '',
    marqueePace: 0.6,//滚动速度
    marqueeDistance: 0,//初始滚动距离
    marquee2copy_status: false,
    //marquee2_margin: 200,//循环首尾间距px
    size: 35,//字体大小rpx
    orientation: 'left',//滚动方向
    interval: 20,// 时间间隔

    //数据信息
    goods: {
      detail: {
        name: "商品名称",
        img_url: "/images/goo.png",
        brief_description: "关于本商品的描述",
        unit_price: 1.00,
      },
      num: 1,
    },
    need: {
      balance: "5.00",
      times: 1,
      wechatpay: "5.00",
    },

    //当前账户信息
    now: {
      balance: 10,
      has_times_cards: [
        { card_name: '白金VIP卡', rest_times: 3, card_id: 1 },
        { card_name: '黄金VIP卡', rest_times: 0, card_id: 2 },
        { card_name: '钻石VIP卡', rest_times: 0, card_id: 3 },
      ],
      no_times_cards: [
        { card_name: '白金VIP卡', rest_times: 3, card_id: 1 },
        { card_name: '黄金VIP卡', rest_times: 0, card_id: 2 },
        { card_name: '钻石VIP卡', rest_times: 0, card_id: 3 },
      ],
    },
    selectcard: '',
    selectcard_id: '',

    //付款方式信息
    way: [
      { name: 'balance', value: 1, disabled: '', checked: '' },
      { name: 'times', value: 2, disabled: '', checked: '' },
      { name: 'wechatpay', value: 3, disabled: '', checked: '' }
    ],
    selectway: undefined,

    //支付过程
    total_to_pay_money: 0,
    order_id: '',
    has_dec_main_balance: '',
    has_dec_second_balance: '',

    showModalStatus: false,
  },

  goback: function () {
    wx.redirectTo({
      url: '/pages/index/index'
    })
  },

  recharge: function () {
    wx.navigateTo({
      url: '/pages/recharge/recharge'
    })
  },

  paymoney: function () {
    var that = this
    console.log("selectcard_id", this.data.selectcard_id)
    if (this.data.selectway == undefined) {
      wx.showModal({
        title: '提示',
        showCancel: false,
        content: '请选择一种支付方式',
        success: function (res) { }
      })
    }
    else {
      console.log("选择第", this.data.selectway, "种支付方式")
      var goods_arr_tmp = [
        {
          goods_type: 1,
          goods_id: that.data.goods.detail.id,
          goods_name: that.data.goods.detail.name,
          unit_price: that.data.goods.detail.unit_price,
          original_price: that.data.goods.detail.original_price,
          discount: that.data.goods.detail.discount,
          quantity: that.data.goods.num,
          note: "扫码扣次",
          company_id: getApp().globalData.settings.company_id,
          shop_id: getApp().globalData.settings.shop_id,
          photo_url: that.data.goods.detail.img_url,
        }
      ]
      console.log("goods_arr", goods_arr_tmp)

      if (that.data.selectway == 1 || that.data.selectway == 3) {
        //创建订单，获得待支付总金额
        wx.request({
          url: getApp().globalData.server + '/API/Shopping/do_order',
          data: {
            shop_id: getApp().globalData.settings.shop_id,
            shop_name: getApp().globalData.settings.shop_name,
            company_id: getApp().globalData.settings.company_id,
            user_id: getApp().globalData.userInfo_detail.user_id,
            username: getApp().globalData.userInfo_detail.username,
            note: "扫码扣次",
            order_type: 1,
            goods_arr: JSON.stringify(goods_arr_tmp),
          },
          method: "POST",
          header: {
            "Content-Type": "application/x-www-form-urlencoded"
          },
          success: function (res) {
            console.log('创建订单，获得待支付总金额', res.data)
            if (res.data.error_no != 0) {
              wx.showModal({
                title: '哎呀～',
                content: '出错了呢！' + res.data.data.error_msg,
                success: function (res) {
                  if (res.confirm) {
                    console.log('用户点击确定')
                  } else if (res.cancel) {
                    console.log('用户点击取消')
                  }
                }
              })
            }
            else if (res.data.error_no == 0) {
              that.data.total_to_pay_money = res.data.data.total_to_pay_money
              console.log('需支付金额（total_to_pay_money）：', that.data.total_to_pay_money)
              that.data.order_id = res.data.data.order_id
              console.log('订单号（order_id）：', that.data.order_id)
              if (that.data.selectway == 1) {
                //余额支付

                //扣除指定用户的指定余额
                wx.request({
                  url: getApp().globalData.server + '/API/User/dec_balance',
                  data: {
                    shop_id: getApp().globalData.settings.shop_id,
                    company_id: getApp().globalData.settings.company_id,
                    user_id: getApp().globalData.userInfo_detail.user_id,
                    to_dec_money: that.data.total_to_pay_money,
                    is_need_confirm_order: 1,
                    order_id: that.data.order_id,
                  },
                  method: "POST",
                  header: {
                    "Content-Type": "application/x-www-form-urlencoded"
                  },
                  success: function (res) {
                    console.log('扣除指定用户的指定余额：', res.data)
                    if (res.data.error_no != 0) {
                      wx.showModal({
                        title: '哎呀～',
                        content: '出错了呢！' + res.data.data.error_msg,
                        success: function (res) {
                          if (res.confirm) {
                            console.log('用户点击确定')
                          } else if (res.cancel) {
                            console.log('用户点击取消')
                          }
                        }
                      })
                    }
                    else if (res.data.error_no == 0) {
                      that.data.has_dec_main_balance = res.data.data.has_dec_main_balance
                      console.log('主账户余额支付的金额：', that.data.has_dec_main_balance)
                      that.data.has_dec_second_balance = res.data.data.has_dec_second_balance
                      console.log('副账户余额支付的金额：', that.data.has_dec_second_balance)

                      wx.showModal({
                        title: '恭喜',
                        showCancel: false,
                        content: '支付成功！',
                        success: function (res) {

                        },
                        complete: function (res) {
                          wx.reLaunch({
                            url: '../myorder/myorder'
                          })
                        }
                      })

                      // 确认订单为已支付
                      // wx.request({
                      //   url: getApp().globalData.server + '/API/Shopping/confirm_order',
                      //   data: {
                      //     shop_id: getApp().globalData.settings.shop_id,
                      //     company_id: getApp().globalData.settings.company_id,
                      //     user_id: getApp().globalData.userInfo_detail.user_id,
                      //     order_id: that.data.order_id,
                      //     main_balance_pay_money: that.data.has_dec_main_balance,
                      //     second_balance_pay_money: that.data.has_dec_second_balance,
                      //   },
                      //   method: "POST",
                      //   header: {
                      //     "Content-Type": "application/x-www-form-urlencoded"
                      //   },
                      //   success: function (res) {
                      //     console.log('确认订单为已支付', res.data)
                      //     if (res.data.error_no != 0) {
                      //       wx.showModal({
                      //         title: '哎呀～',
                      //         content: '出错了呢！' + res.data.data.error_msg,
                      //         success: function (res) {
                      //           if (res.confirm) {
                      //             console.log('用户点击确定')
                      //           } else if (res.cancel) {
                      //             console.log('用户点击取消')
                      //           }
                      //         }
                      //       })
                      //     }
                      //     else if (res.data.error_no == 0) {
                      //       wx.showModal({
                      //         title: '恭喜',
                      //         showCancel: false,
                      //         content: '支付成功！',
                      //         success: function (res) {
                      //           
                      //         },
                      //         complete: function (res) {
                      //           wx.reLaunch({
                      //             url: '../load/load'
                      //           })
                      //         }
                      //       })
                      //     }
                      //   },
                      //   fail: function (res) {
                      //     wx.showModal({
                      //       title: '哎呀～',
                      //       content: '网络不在状态呢！',
                      //       success: function (res) {
                      //         if (res.confirm) {
                      //           console.log('用户点击确定')
                      //         } else if (res.cancel) {
                      //           console.log('用户点击取消')
                      //         }
                      //       }
                      //     })
                      //   }
                      // })
                    }
                  },
                  fail: function (res) {
                    wx.showModal({
                      title: '哎呀～',
                      content: '网络不在状态呢！',
                      success: function (res) {
                        if (res.confirm) {
                          console.log('用户点击确定')
                        } else if (res.cancel) {
                          console.log('用户点击取消')
                        }
                      }
                    })
                  }
                })
              }
              else if (that.data.selectway == 3) {
                //微信支付
                wx.login({
                  success: function (res) {
                    if (res.code) {
                      console.log('code', res.code)
                      wx.request({
                        url: getApp().globalData.server + "/API/Pay/pay_request",
                        data: {
                          code: res.code,
                          company_id: getApp().globalData.settings.company_id,
                          shop_id: getApp().globalData.settings.shop_id,
                          order_id: that.data.order_id,
                          total_to_pay_money: that.data.total_to_pay_money,
                          user_id: getApp().globalData.userInfo_detail.user_id
                        },
                        method: "POST",
                        header: {
                          "Content-Type": "application/x-www-form-urlencoded"
                        },
                        success: function (res2) {
                          console.log(res2)
                          if (res2.data.error_no != 0) {
                            wx.showModal({
                              title: '哎呀',
                              content: '请求支付失败！',
                              success: function (res) {
                                if (res.confirm) {
                                  console.log('用户点击确定')
                                } else if (res.cancel) {
                                  console.log('用户点击取消')
                                }
                              }
                            })
                          }
                          else if (res2.data.error_no == 0) {
                            var pay = require("../pay/pay.js").pay
                            pay(res2, that, 2)
                          }
                        },
                        fail: function (res) {
                          wx.showModal({
                            title: '哎呀～',
                            content: '请求支付失败！',
                            success: function (res) {
                              if (res.confirm) {
                                console.log('用户点击确定')
                              } else if (res.cancel) {
                                console.log('用户点击取消')
                              }
                            }
                          })
                        }
                      })
                    } else {
                      console.log('登录失败！' + res.errMsg)
                      wx.showModal({
                        title: '哎呀～',
                        content: '内部错误1',
                        success: function (res) {
                          if (res.confirm) {
                            console.log('用户点击确定')
                          } else if (res.cancel) {
                            console.log('用户点击取消')
                          }
                        }
                      })
                    }
                  }
                });
              }
            }
          },
          fail: function (res) {
            wx.showModal({
              title: '哎呀～',
              content: '网络不在状态呢！',
              success: function (res) {
                if (res.confirm) {
                  console.log('用户点击确定')
                } else if (res.cancel) {
                  console.log('用户点击取消')
                }
              }
            })
          }
        })
      }
      else if (that.data.selectway == 2) {
        //扣卡支付
        var data = {
          shop_id: getApp().globalData.settings.shop_id,
          company_id: getApp().globalData.settings.company_id,
          user_id: getApp().globalData.userInfo_detail.user_id,
          to_dec_times: that.data.goods.num,
          times_card_record_id: that.data.selectcard_id,
          goods_id: that.data.goods_id,
          goods_name: that.data.goods.detail.name,
        }
        console.log(data)
        wx.request({
          url: getApp().globalData.server + '/API/Card/dec_one_times_card',
          data: {
            shop_id: getApp().globalData.settings.shop_id,
            company_id: getApp().globalData.settings.company_id,
            user_id: getApp().globalData.userInfo_detail.user_id,
            to_dec_times: that.data.goods.num,
            times_card_record_id: that.data.selectcard_id,
            goods_id: that.data.goods_id,
            goods_name: that.data.goods.detail.name,
          },
          method: "POST",
          header: {
            "Content-Type": "application/x-www-form-urlencoded"
          },
          success: function (res) {
            console.log('扣卡支付', res.data)
            if (res.data.error_no != 0) {
              wx.showModal({
                title: '哎呀～',
                content: '出错了呢！' + res.data.data.error_msg,
                success: function (res) {
                  if (res.confirm) {
                    console.log('用户点击确定')
                  } else if (res.cancel) {
                    console.log('用户点击取消')
                  }
                }
              })
            }
            else if (res.data.error_no == 0) {
              wx.showModal({
                title: '恭喜',
                showCancel: false,
                content: '支付成功！',
                success: function (res) {

                },
                complete: function (res) {
                  wx.reLaunch({
                    url: '../myorder/myorder'
                  })
                }
              })
            }
          },
          fail: function (res) {
            wx.showModal({
              title: '哎呀～',
              content: '网络不在状态呢！',
              success: function (res) {
                if (res.confirm) {
                  console.log('用户点击确定')
                } else if (res.cancel) {
                  console.log('用户点击取消')
                }
              }
            })
          }
        })
      }
    }
  },

  onTabsItemTap: function (event) {
    var that = this;
    var currentStatu = "close";
    this.util(currentStatu)
    var index = event.currentTarget.dataset['index'];
    this.setData({
      selectcard: this.data.now.has_times_cards[index].card_name + " 剩余" + this.data.now.has_times_cards[index].rest_times + "次",
      selectcard_id: this.data.now.has_times_cards[index].id,
    })
    if (this.data.now.has_times_cards[index].rest_times == 0) {
      var tem1 = "way[1].checked"
      var tem2 = "way[1].disabled"
      that.setData({
        [tem1]: '',
        [tem2]: 'true',
      })
    }
    else {
      var tem1 = "way[1].disabled"
      var tem2 = "way[1].checked"
      var tem3 = "way[0].checked"
      var tem4 = "way[2].checked"
      that.setData({
        [tem1]: '',
        [tem2]: 'true',
        [tem3]: '',
        [tem4]: '',
        selectway: 2
      })
    }
  },

  powerDrawer: function (e) {
    var currentStatu = e.currentTarget.dataset.statu;
    this.util(currentStatu)
  },

  util: function (currentStatu) {
    /* 动画部分 */
    // 第1步：创建动画实例   
    var animation = wx.createAnimation({
      duration: 200,  //动画时长  
      timingFunction: "linear", //线性  
      delay: 0  //0则不延迟  
    });

    // 第2步：这个动画实例赋给当前的动画实例  
    this.animation = animation;

    // 第3步：执行第一组动画：Y轴偏移240px后(盒子高度是240px)，停  
    animation.translateY(240).step();

    // 第4步：导出动画对象赋给数据对象储存  
    this.setData({
      animationData: animation.export()
    })

    // 第5步：设置定时器到指定时候后，执行第二组动画  
    setTimeout(function () {
      // 执行第二组动画：Y轴不偏移，停  
      animation.translateY(0).step()
      // 给数据对象储存的第一组动画，更替为执行完第二组动画的动画对象  
      this.setData({
        animationData: animation
      })

      //关闭抽屉  
      if (currentStatu == "close") {
        this.setData(
          {
            showModalStatus: false
          }
        );
      }
    }.bind(this), 200)

    // 显示抽屉  
    if (currentStatu == "open") {
      this.setData(
        {
          showModalStatus: true
        }
      );
    }
  },

  /**
   * 监听checkbox事件
   */
  listenCheckboxChange: function (e) {
    var that = this;
    console.log(e)
    if (e.detail.value.length >= 2) {
      var tem = "way[" + [e.detail.value[0] - 1] + "].checked"
      that.setData({
        [tem]: '',
      })
      console.log('checkbox发生change事件，携带value值为：', e.detail.value[1])
      that.setData({
        selectway: e.detail.value[1],
      })
    }
    else {
      console.log('checkbox发生change事件，携带value值为：', e.detail.value[0])
      that.setData({
        selectway: e.detail.value[0],
      })
    }
  },

  run: function () {
    var vm = this;
    var interval = setInterval(function () {
      if (-vm.data.marqueeDistance < vm.data.length) {
        vm.setData({
          marqueeDistance: vm.data.marqueeDistance - vm.data.marqueePace,
        });
      } else {
        clearInterval(interval);
        vm.setData({
          marqueeDistance: vm.data.windowWidth
        });
        vm.run();
      }
    }, vm.data.interval);
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log("页面跳转传递", options)
    this.setData({
      goods_id: options.goods_id,
      shop_name: getApp().globalData.settings.shop_name,
      is_use_payment: getApp().globalData.settings.is_use_payment,
    })

    var that = this

    //获取指定商品详情
    wx.request({
      url: getApp().globalData.server + '/API/Goods/get_one_goods',
      data: {
        shop_id: getApp().globalData.settings.shop_id,
        company_id: getApp().globalData.settings.company_id,
        goods_id: that.data.goods_id,
      },
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function (res) {
        console.log('获取指定商品详情', res.data)
        if (res.data.error_no != 0) {
          wx.showModal({
            title: '哎呀～',
            content: '出错了呢！' + res.data.data.error_msg,
            success: function (res) {
              if (res.confirm) {
                console.log('用户点击确定')
              } else if (res.cancel) {
                console.log('用户点击取消')
              }
            }
          })
        }
        else if (res.data.error_no == 0) {
          var tem1 = "need.balance"
          that.setData({
            [tem1]: res.data.data.unit_price
          })
          var tem2 = "need.wechatpay"
          that.setData({
            [tem2]: res.data.data.unit_price
          })
          var tem3 = "goods.detail"
          that.setData({
            [tem3]: res.data.data
          })
        }
      },
      fail: function (res) {
        wx.showModal({
          title: '哎呀～',
          content: '网络不在状态呢！',
          success: function (res) {
            if (res.confirm) {
              console.log('用户点击确定')
            } else if (res.cancel) {
              console.log('用户点击取消')
            }
          }
        })
      }
    })

    //根据用户id得到其账户的剩余金额
    wx.request({
      url: getApp().globalData.server + '/API/User/get_balance_by_user_id',
      data: {
        shop_id: getApp().globalData.settings.shop_id,
        company_id: getApp().globalData.settings.company_id,
        user_id: getApp().globalData.userInfo_detail.user_id,
      },
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function (res) {
        console.log('根据用户id得到其账户的剩余金额', res.data)
        if (res.data.error_no != 0) {
          wx.showModal({
            title: '哎呀～',
            content: '出错了呢！' + res.data.data.error_msg,
            success: function (res) {
              if (res.confirm) {
                console.log('用户点击确定')
              } else if (res.cancel) {
                console.log('用户点击取消')
              }
            }
          })
        }
        else if (res.data.error_no == 0) {
          var tem = "now.balance"
          that.setData({
            [tem]: res.data.data.total_balance
          })
          if (res.data.data.total_balance == 0) {
            var batem = "way[0].disabled"
            that.setData({
              [batem]: 'true',
            })
          }
        }
      },
      fail: function (res) {
        wx.showModal({
          title: '哎呀～',
          content: '网络不在状态呢！',
          success: function (res) {
            if (res.confirm) {
              console.log('用户点击确定')
            } else if (res.cancel) {
              console.log('用户点击取消')
            }
          }
        })
      }
    })

    // 根据商品id获得指定用户适用的所有卡
    wx.request({
      url: getApp().globalData.server + '/API/Card/get_one_user_all_cards_by_one_goods_id',
      data: {
        shop_id: getApp().globalData.settings.shop_id,
        company_id: getApp().globalData.settings.company_id,
        user_id: getApp().globalData.userInfo_detail.user_id,
        goods_id: that.data.goods_id,
      },
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function (res) {
        console.log('根据商品id获得指定用户适用的所有卡', res.data)
        if (res.data.error_no != 0) {
          wx.showModal({
            title: '哎呀～',
            content: '出错了呢！' + res.data.data.error_msg,
            success: function (res) {
              if (res.confirm) {
                console.log('用户点击确定')
              } else if (res.cancel) {
                console.log('用户点击取消')
              }
            }
          })
        }
        else if (res.data.error_no == 0) {
          var tem1 = "now.has_times_cards"
          var tem2 = "now.no_times_cards"
          that.setData({
            [tem1]: [],
            [tem2]: [],
          })
          var i, j = 0
          for (i = 0; i < res.data.data.times_card.has_times_cards.length; i++) {
            var tem = "now.has_times_cards[" + i + "]"
            that.setData({
              [tem]: res.data.data.times_card.has_times_cards[i]
            })
          }
          for (j = 0; j < res.data.data.times_card.no_times_cards.length; j++) {
            var tem = "now.no_times_cards[" + j + "]"
            that.setData({
              [tem]: res.data.data.times_card.no_times_cards[j]
            })
          }
          console.log('可用卡：', that.data.now.has_times_cards)
          console.log('不可用卡：', that.data.now.no_times_cards)
          if (res.data.data.times_card.has_times_cards.length > 0) {
            that.setData({
              selectcard: that.data.now.has_times_cards[0].card_name + " 剩余" + that.data.now.has_times_cards[0].rest_times + "次",
              selectcard_id: that.data.now.has_times_cards[0].id,
            })
          }
          else if (res.data.data.times_card.no_times_cards.length > 0) {
            that.setData({
              selectcard: that.data.now.no_times_cards[0].card_name + " 剩余" + that.data.now.no_times_cards[0].rest_times + "次",
              selectcard_id: that.data.now.no_times_cards[0].id,
            })
            var tem1 = "way[1].checked"
            var tem2 = "way[1].disabled"
            that.setData({
              [tem1]: '',
              [tem2]: 'true',
            })
          }
          else {
            that.setData({
              selectcard: "无可用卡",
              selectcard_id: null,
            })
            var tem1 = "way[1].checked"
            var tem2 = "way[1].disabled"
            that.setData({
              [tem1]: '',
              [tem2]: 'true',
            })
          }
        }
      },
      fail: function (res) {
        wx.showModal({
          title: '哎呀～',
          content: '网络不在状态呢！',
          success: function (res) {
            if (res.confirm) {
              console.log('用户点击确定')
            } else if (res.cancel) {
              console.log('用户点击取消')
            }
          }
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    this.setData({
      is_use_payment: getApp().globalData.settings.is_use_payment,
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    // 页面显示
    var vm = this;
    var length = vm.data.text.length * vm.data.size;//文字长度
    var windowWidth = wx.getSystemInfoSync().windowWidth;// 屏幕宽度
    vm.setData({
      length: length,
      windowWidth: windowWidth,
      marquee2_margin: length < windowWidth ? windowWidth - length : vm.data.marquee2_margin//当文字长度小于屏幕长度时，需要增加补白
    });
    vm.run();// 第一个字消失后立即从右边出现

    vm.setData({
      text: '来果果爱，就知道果果怎么爱你的....',
      is_use_payment: getApp().globalData.settings.is_use_payment,
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return (util.share())
  }
})
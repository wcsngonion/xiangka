// pages/lottery/lottery.js
var app = getApp()
var util = require('../../utils/util.js');

Page({

  /**
   * 页面的初始数据
   */
  data: {
    circleList: [], //圆点数组  
    awardList: [], //奖品数组  
    colorCircleFirst: '#FFDF2F', //圆点颜色1
    colorCircleSecond: '#FE4D32', //圆点颜色2  
    colorAwardDefault: '#b5d9fa', //奖品默认颜色  
    colorAwardSelect: '#ffe400', //奖品选中颜色  
    indexSelect: 0, //被选中的奖品index  
    isRunning: false, //是否正在抽奖  
    imageAward: [
      '../../images/1.jpg',
      '../../images/2.jpg',
      '../../images/3.jpg',
      '../../images/4.jpg',
      '../../images/5.jpg',
      '../../images/6.jpg',
      '../../images/7.jpg',
      '../../images/8.jpg',
    ], //奖品图片数组  

    each_lottery_cost_points: '',
    allow_lottery_times_each_day: '',
    advertisement_photo_url: '',
    rest_times_txt: '每日允许最多抽奖次数',
    rest_points: '0',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.setData({
      rest_points: getApp().globalData.userInfo_detail.point,
    })
    var _this = this;
    //圆点设置  
    var leftCircle = 7.5;
    var topCircle = 7.5;
    var circleList = [];
    for (var i = 0; i < 24; i++) {
      if (i == 0) {
        topCircle = 15;
        leftCircle = 15;
      } else if (i < 6) {
        topCircle = 7.5;
        leftCircle = leftCircle + 102.5;
      } else if (i == 6) {
        topCircle = 15
        leftCircle = 620;
      } else if (i < 12) {
        topCircle = topCircle + 94;
        leftCircle = 620;
      } else if (i == 12) {
        topCircle = 565;
        leftCircle = 620;
      } else if (i < 18) {
        topCircle = 570;
        leftCircle = leftCircle - 102.5;
      } else if (i == 18) {
        topCircle = 565;
        leftCircle = 15;
      } else if (i < 24) {
        topCircle = topCircle - 94;
        leftCircle = 7.5;
      } else {
        return
      }
      circleList.push({
        topCircle: topCircle,
        leftCircle: leftCircle
      });
    }
    this.setData({
      circleList: circleList
    })

    //圆点闪烁  
    setInterval(function() {
      if (_this.data.colorCircleFirst == '#FFDF2F') {
        _this.setData({
          colorCircleFirst: '#FE4D32',
          colorCircleSecond: '#FFDF2F',
        })
      } else {
        _this.setData({
          colorCircleFirst: '#FFDF2F',
          colorCircleSecond: '#FE4D32',
        })
      }
    }, 500) //设置圆点闪烁的效果  

    //奖品item设置  
    var awardList = [];
    //间距,怎么顺眼怎么设置吧.  
    var topAward = 25;
    var leftAward = 25;
    for (var j = 0; j < 8; j++) {
      if (j == 0) {
        topAward = 25;
        leftAward = 25;
      } else if (j < 3) {
        topAward = topAward;
        //166.6666是宽.15是间距.下同  
        leftAward = leftAward + 166.6666 + 15;
      } else if (j < 5) {
        leftAward = leftAward;
        //150是高,15是间距,下同  
        topAward = topAward + 150 + 15;
      } else if (j < 7) {
        leftAward = leftAward - 166.6666 - 15;
        topAward = topAward;
      } else if (j < 8) {
        leftAward = leftAward;
        topAward = topAward - 150 - 15;
      }
      var imageAward = this.data.imageAward[j];
      awardList.push({
        topAward: topAward,
        leftAward: leftAward,
        imageAward: imageAward
      });
    }
    this.setData({
      awardList: awardList
    })

    //得到抽奖奖品列表
    wx.request({
      url: getApp().globalData.server + '/API/Lottery/get_lottery_all_prizes',
      data: {
        shop_id: getApp().globalData.settings.shop_id,
        company_id: getApp().globalData.settings.company_id,
      },
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function(res) {
        console.log('得到抽奖奖品列表', res.data)
        if (res.data.error_no != 0) {
          wx.showModal({
            title: '哎呀～',
            content: '出错了呢！' + res.data.data.error_msg,
            success: function(res) {
              if (res.confirm) {
                console.log('用户点击确定')
              } else if (res.cancel) {
                console.log('用户点击取消')
              }
            }
          })
        } else if (res.data.error_no == 0) {
          var ntem = res.data.data.prizes
          for (var j = 0; j < ntem.length; j++) {
            awardList[j].name = ntem[j].name
            awardList[j].no = ntem[j].no
          }
          console.log(awardList)
          _this.setData({
            awardList: awardList,
            each_lottery_cost_points: res.data.data.each_lottery_cost_points,
            allow_lottery_times_each_day: res.data.data.allow_lottery_times_each_day,
            advertisement_photo_url: res.data.data.advertisement_photo_url,
          })
        }
      },
      fail: function(res) {
        wx.showModal({
          title: '哎呀～',
          content: '网络不在状态呢！',
          success: function(res) {
            if (res.confirm) {
              console.log('用户点击确定')
            } else if (res.cancel) {
              console.log('用户点击取消')
            }
          }
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {
    this.setData({
      rest_points: getApp().globalData.userInfo_detail.point,
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    this.setData({
      rest_points: getApp().globalData.userInfo_detail.point,
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {
    return (util.share())
  },

  //开始抽奖  
  formSubmit: function(e) {
    var that = this

    if (this.data.isRunning) return
    this.setData({
      isRunning: true
    })
    var _this = this;
    var indexSelect = -1
    var i = 0;

    //执行抽奖
    wx.request({
      url: getApp().globalData.server + '/API/Lottery/do_lottery',
      data: {
        shop_id: getApp().globalData.settings.shop_id,
        company_id: getApp().globalData.settings.company_id,
        user_id: getApp().globalData.userInfo_detail.user_id,
      },
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function(res) {
        console.log('执行抽奖', res.data)
        if (res.data.error_no != 0) {
          wx.showModal({
            title: '哎呀～',
            content: '出错了呢！' + res.data.data.error_msg,
            success: function(res) {
              if (res.confirm) {
                console.log('用户点击确定')
              } else if (res.cancel) {
                console.log('用户点击取消')
              }
            },
            complete: function() {
              _this.setData({
                isRunning: false
              })
            }
          })
        } else if (res.data.error_no == 0) {
          var rest_times_txt = '今日还允许抽奖次数';
          _this.setData({
            rest_times_txt: rest_times_txt,
            allow_lottery_times_each_day: res.data.data.remain_lottery_times,
            rest_points: res.data.data.current_point,
          })
          getApp().globalData.userInfo_detail.point = res.data.data.current_point

          var timer = setInterval(function() {
            indexSelect++;
            indexSelect = indexSelect % 8;

            //根据需求改变转盘速度  
            i += 80;
            if (i > 2000 + 1000 * Math.random() && indexSelect == (res.data.data.award_prize.no - 1)) {
              //去除循环  
              clearInterval(timer)
              //获奖提示  

              wx.showModal({
                title: res.data.data.award_prize.goods_name,
                content: '小提示：中奖后请前往“我的中奖”领取~',
                showCancel: false, //去掉取消按钮  
                success: function(res) {
                  if (res.confirm) {}
                },
                complete: function() {
                  _this.setData({
                    isRunning: false
                  })
                }
              })
            }
            _this.setData({
              indexSelect: indexSelect
            })
          }, (200 + 40 * i))

          console.log('积分变动微信提醒(formId)：', e.detail.formId)
          //积分变动微信提醒
          wx.request({
            url: getApp().globalData.server + '/API/Point/point_wechat_notify',
            data: {
              company_id: getApp().globalData.settings.company_id,
              shop_id: getApp().globalData.settings.shop_id,
              user_id: getApp().globalData.userInfo_detail.user_id,
              username: getApp().globalData.userInfo_detail.username,
              openid: getApp().globalData.open_id,
              form_id: e.detail.formId,
              points_type: '扣除',
              points_from: '积分抽奖',
              consume_points: res.data.data.has_change_point,
              total_points: res.data.data.current_point,
            },
            method: "POST",
            header: {
              "Content-Type": "application/x-www-form-urlencoded"
            },
            success: function(res) {
              console.log('积分变动微信提醒', res.data)
              if (res.data.error_no != 0) {
                // wx.showModal({
                //   title: '哎呀～',
                //   content: '出错了呢！' + res.data.data.error_msg,
                //   success: function (res) {
                //     if (res.confirm) {
                //       console.log('用户点击确定')
                //     } else if (res.cancel) {
                //       console.log('用户点击取消')
                //     }
                //   }
                // })
              } else if (res.data.error_no == 0) {}
            },
            fail: function(res) {
              // wx.showModal({
              //   title: '哎呀～',
              //   content: '网络不在状态呢！',
              //   success: function (res) {
              //     if (res.confirm) {
              //       console.log('用户点击确定')
              //     } else if (res.cancel) {
              //       console.log('用户点击取消')
              //     }
              //   }
              // })
            }
          })
        }
      },
      fail: function(res) {
        wx.showModal({
          title: '哎呀～',
          content: '网络不在状态呢！',
          success: function(res) {
            if (res.confirm) {
              console.log('用户点击确定')
            } else if (res.cancel) {
              console.log('用户点击取消')
            }
          }
        })
      }
    })
  },
  prize: function() {
    wx.navigateTo({
      url: '../myprize/myprize',
    })
  },
})
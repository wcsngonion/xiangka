// pages/sharing/sharing.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    username: "会员卡",//用户名称
    userphoto: getApp().globalData.userInfo.avatarUrl,//用户头像

    share_miniapp: "0",
    friend_open_my_add: "0",
    friend_open_his_add: "0",
    friend_register_my_add: "0",
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this

    this.setData({
      userphoto: getApp().globalData.userInfo.avatarUrl,
    })

    //获取分享积分规则
    wx.request({
      url: getApp().globalData.server + '/API/Point/get_share_point_rule_by_company',
      data: {
        shop_id: getApp().globalData.settings.shop_id,
        company_id: getApp().globalData.settings.company_id,
      },
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function (res) {
        console.log('获取分享积分规则', res.data)
        if (res.data.error_no != 0) {
          wx.showModal({
            title: '哎呀～',
            content: '出错了呢！' + res.data.data.error_msg,
            success: function (res) {
              if (res.confirm) {
                console.log('用户点击确定')
              } else if (res.cancel) {
                console.log('用户点击取消')
              }
            }
          })
        }
        else if (res.data.error_no == 0) {
          that.setData({
            share_miniapp: res.data.data.share_rules.share_miniapp,
            friend_open_my_add: res.data.data.share_rules.friend_open_my_add,
            friend_open_his_add: res.data.data.share_rules.friend_open_his_add,
            friend_register_my_add: res.data.data.share_rules.friend_register_my_add,
          })
        }
      },
      fail: function (res) {
        wx.showModal({
          title: '哎呀～',
          content: '网络不在状态呢！',
          success: function (res) {
            if (res.confirm) {
              console.log('用户点击确定')
            } else if (res.cancel) {
              console.log('用户点击取消')
            }
          }
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    this.setData({
      userphoto: getApp().globalData.userInfo.avatarUrl,
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    if (getApp().globalData.userInfo != "") {
      this.setData({
        userInfo: getApp().globalData.userInfo,
        userphoto: getApp().globalData.userInfo.avatarUrl
      })
    }
    else {
      this.setData({
        userphoto: "/images/pho.png",
      })
    }
    if (getApp().globalData.userInfo_detail.is_phone_login == 1) {
      this.setData({
        username: getApp().globalData.userInfo_detail.username,
      })
    }
    else {
      this.setData({
        username: getApp().globalData.userInfo.nickName,
      })
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (res) {
    if (res.from === 'button') {
      // 来自页面内转发按钮
      console.log(res.target)
    }
    return {
      title: '接受邀请，赢取积分，兑换好礼！',
      path: '/pages/load/load?share_user=' + getApp().globalData.userInfo_detail.user_id,
      success: function (res) {
        // 转发成功
        wx.request({
          url: getApp().globalData.server + '/API/Point/share_successfully',
          data: {
            shop_id: getApp().globalData.settings.shop_id,
            company_id: getApp().globalData.settings.company_id,
            user_id: getApp().globalData.userInfo_detail.user_id,
          },
          method: "POST",
          header: {
            "Content-Type": "application/x-www-form-urlencoded"
          },
          success: function (res) {
            console.log('分享成功增加积分', res.data)
            if (res.data.error_no != 0) {
              wx.showModal({
                title: '哎呀～',
                content: '出错了呢！' + res.data.data.error_msg,
                success: function (res) {
                  if (res.confirm) {
                    console.log('用户点击确定')
                  } else if (res.cancel) {
                    console.log('用户点击取消')
                  }
                }
              })
            }
            else if (res.data.error_no == 0) {
              getApp().globalData.userInfo_detail.point = res.data.data.current_point
              wx.showModal({
                title: '恭喜',
                showCancel: false,
                content: '分享成功，恭喜增加 ' + res.data.data.add_point + ' 个积分！可兑换好礼哦~',
                success: function (res) {
                  if (res.confirm) {
                    console.log('用户点击确定')
                  }
                },
                complete: function (res) {
                  wx.navigateBack({
                    delta: 1
                  })
                }
              })
            }
          },
          fail: function (res) {
            wx.showModal({
              title: '哎呀～',
              content: '网络不在状态呢！',
              success: function (res) {
                if (res.confirm) {
                  console.log('用户点击确定')
                } else if (res.cancel) {
                  console.log('用户点击取消')
                }
              }
            })
          }
        })
      },
      fail: function (res) {
        // 转发失败
        wx.showModal({
          title: '提示',
          showCancel: false,
          content: '分享失败！',
          success: function (res) {
            if (res.confirm) {
              console.log('用户点击确定')
            }
          }
        })
      }
    }
  }
})
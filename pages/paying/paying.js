var app = getApp()

// 这部分代码需要解决以下问题
// 输入的金额在消费金额中显示出来
// 折扣券和代金券只能二选一
// 点击选择折扣券和选择代金券框内打钩并显示优惠金额

Page({
  data: {
    shop_name: '',
    flag: 0,
    rmb_img: '/images/RMB.png',
    show: true, //解决备注中文字在弹窗前的问题
    content: "", //在点击弹窗后将文字保存在这里
    consumption: 0, //消费金额
    discount: 1, //使用折扣券得到的折扣
    discountVoucher: 0,
    consumption_0: 0, //确认买单
    boxstate: 0, //盒子状态
    consumption_after_discount: 0, //使用折扣券之后的金额
    youhui: 0,
    youhuiInVoucher: 0,
    company_name: "", //店铺名字
    opacity: 1, //确认买单透明度
    showModalStatus: false, //抽屉
    showModalStatusVoucher: false, //放代金券的抽屉
    balance_pay: 0,
    disabled: 0,
    selectcard: '点我选择折扣券', //选定卡的名字
    selectcardVoucher: '点我选择代金券',
    discount_card_id: '', //选择卡的id
    discountID_temp: '', //在没选中优惠券框的时候选择优惠券会暂时存在这里
    voucher_card_id: '',
    voucherID_temp: '',
    beizhu: "", //备注
    yuedisplay: 0, //为了余额支付那里显示设置的值
    xiaofeijine: '', //消费金额那个框的数值
    //当前账户信息

    can_use_cards: [], //能被使用的卡券
    cannot_use_cards: [], //不能被使用的卡券

    //数据信息


    //当前账户信息
    now: {
      balance: 0,

    },


    //付款方式信息
    way: [{
        name: 'balance',
        value: 1,
        disabled: '',
        checked: ''
      },
      {
        name: 'coupons',
        value: 2,
        disabled: '',
        checked: ''
      },
      {
        name: 'voucher',
        value: 3,
        disabled: '',
        checked: ''
      }
    ],


    //支付过程
    total_to_pay_money: 0, //总消费付款
    order_id: '', //订单id
    has_dec_main_balance: '', //已经扣除主账户余额
    has_dec_second_balance: '', //已经扣除的副账户余额

    showModalStatus: false, //

    //yi add
    dis: {
      name: 'disc',
      value: 4,
      disabled: '',
      checked: ''
    },
    discount_weekday: {},
    isdisc: false,
    minput: "0",
    //yi end
  },

  //yi start
  isusediscount: function(e) {
    this.data.dis.checked = !this.data.dis.checked
    console.log("是否使用当日优惠", this.data.dis.checked)
    this.data.isdisc = this.data.dis.checked
    console

    var that = this
    var tem1 = 'way[1]'
    var tem2 = 'way[2]'
    if (this.data.isdisc == true) {
      that.setData({
        consumption: that.data.minput,
        // consumption_0: money_low,  因为改为输入金额取消掉勾选改为下一行代码
        consumption_0: (that.data.minput * that.data.discount_weekday.discount).toFixed(2),
        yuedisplay: (that.data.minput * that.data.discount_weekday.discount).toFixed(2),
        youhui: 0,
        youhuiInVoucher: 0,
        discount: 1,
        selectcard: '点我选择折扣券',
        selectcardVoucher: '点我选择代金券',
        discount_card_id: '',
        discount_temp: '',
        voucher_card_id: '',
        voucherID_temp: '',
        [tem1]: {
          name: 'coupons',
          value: 2,
          disabled: true,
          checked: false
        },
        [tem2]: {
          name: 'voucher',
          value: 3,
          disabled: true,
          checked: false
        },
      })
    } else {
      that.setData({
        consumption: that.data.minput,
        // consumption_0: money_low,  因为改为输入金额取消掉勾选改为下一行代码
        consumption_0: that.data.minput,
        yuedisplay: that.data.minput,
        youhui: 0,
        youhuiInVoucher: 0,
        discount: 1,
        selectcard: '点我选择折扣券',
        selectcardVoucher: '点我选择代金券',
        discount_card_id: '',
        discount_temp: '',
        voucher_card_id: '',
        voucherID_temp: '',
        [tem1]: {
          name: 'coupons',
          value: 2,
          disabled: '',
          checked: false
        },
        [tem2]: {
          name: 'voucher',
          value: 3,
          disabled: '',
          checked: false
        },
      })
    }
  },
  //yi end

  //确认买单
  confirm: function() {
    console.log(111)
    var that = this
    if (that.data.consumption == 0 || that.data.consumption == '' || that.data.consumption == NaN || that.data.consumption == undefined) {
      wx.showModal({
        title: '哎呀～',
        content: '输入金额不能为零',
        success: function(res) {
          if (res.confirm) {
            console.log('用户点击确定')
          } else if (res.cancel) {
            console.log('用户点击取消')
          }
        }
      })
    } else {
      if (that.data.disabled == 0) {
        wx.login({
          success: function(res) {
            if (res.code) {
              console.log(res.code)
              console.log(that.data.consumption)
              console.log(that.data.voucher_card_id)
              console.log("that.data.balance_pay", that.data.balance_pay)
              if (that.data.isdisc == true) {
                if (that.data.way[0].checked == true) {
                  wx.request({
                    url: getApp().globalData.server + "/API/Shopping/online_pay",
                    data: {
                      user_id: getApp().globalData.userInfo_detail.user_id,
                      username: getApp().globalData.userInfo_detail.username,
                      total_to_pay_money: that.data.consumption,
                      balance_pay_money: that.data.yuedisplay,
                      company_id: getApp().globalData.settings.company_id,
                      shop_id: getApp().globalData.settings.shop_id,
                      shop_name: getApp().globalData.settings.shop_name,
                      note: that.data.beizhu,
                      discount_card_id: that.data.discount_card_id,
                      must_dec_main_balance: 0,
                      voucher_record_id: that.data.voucher_card_id,
                      is_use_today_shop_weekend_discount: 1,
                    },
                    method: "POST",
                    header: {
                      "Content-Type": "application/x-www-form-urlencoded"
                    },
                    success: function(res1) {
                      // console.log(that.data)
                      console.log("res1", res1)
                      if (res1.data.error_no != 0) {
                        wx.showModal({
                          title: '哎呀～',
                          content: '创建订单失败！',
                          success: function(res) {
                            if (res.confirm) {
                              console.log('用户点击确定')
                            } else if (res.cancel) {
                              console.log('用户点击取消')
                            }
                          }
                        })
                      } else if (res1.data.error_no == 0) {
                        if (res1.data.data.to_wechat_pay_money == 0) {
                          wx.showModal({
                            title: '恭喜',
                            showCancel: false,
                            content: '支付成功！',
                            success: function(res) {

                            },
                            complete: function(res) {
                              wx.reLaunch({
                                url: '../myorder/myorder'
                              })
                            }
                          })
                        } else {
                          wx.request({
                            url: getApp().globalData.server + "/API/Pay/pay_request",
                            data: {
                              code: res.code,
                              company_id: getApp().globalData.settings.company_id,
                              shop_id: getApp().globalData.settings.shop_id,
                              order_id: res1.data.data.order_id,
                              total_to_pay_money: res1.data.data.to_wechat_pay_money,
                              user_id: getApp().globalData.userInfo_detail.user_id
                            },
                            method: "POST",
                            header: {
                              "Content-Type": "application/x-www-form-urlencoded"
                            },
                            success: function(res2) {
                              console.log("res2", res2)
                              console.log("pay", res1.data.data.to_wechat_pay_money)
                              if (res2.data.error_no != 0) {
                                wx.showModal({
                                  title: '哎呀',
                                  content: '请求支付失败！' + res2.data.data.error_msg,
                                  success: function(res) {
                                    if (res.confirm) {
                                      console.log('用户点击确定')
                                    } else if (res.cancel) {
                                      console.log('用户点击取消')
                                    }

                                  }
                                })
                              } else if (res2.data.error_no == 0) {
                                var pay = require("../pay/pay.js").pay
                                pay(res2, that, 4)
                              }
                            },
                            fail: function(res) {
                              wx.showModal({
                                title: '哎呀～',
                                content: '请求支付失败！',
                                success: function(res) {
                                  if (res.confirm) {
                                    console.log('用户点击确定')
                                  } else if (res.cancel) {
                                    console.log('用户点击取消')
                                  }
                                },
                                complete: function(res) {
                                  wx.reLaunch({
                                    url: '../myorder/myorder'
                                  })
                                }

                              })
                            }
                          })
                        }
                      }
                    },
                    fail: function(res) {
                      wx.showModal({
                        title: '哎呀～',
                        content: '网络不在状态呢！',
                        success: function(res) {
                          if (res.confirm) {
                            console.log('用户点击确定')
                          } else if (res.cancel) {
                            console.log('用户点击取消')
                          }
                        }
                      })
                    }
                  })
                } else {
                  wx.request({
                    url: getApp().globalData.server + "/API/Shopping/online_pay",
                    data: {
                      user_id: getApp().globalData.userInfo_detail.user_id,
                      username: getApp().globalData.userInfo_detail.username,
                      total_to_pay_money: that.data.consumption,
                      company_id: getApp().globalData.settings.company_id,
                      shop_id: getApp().globalData.settings.shop_id,
                      shop_name: getApp().globalData.settings.shop_name,
                      note: that.data.beizhu,
                      discount_card_id: that.data.discount_card_id,
                      must_dec_main_balance: 0,
                      voucher_record_id: that.data.voucher_card_id,
                      is_use_today_shop_weekend_discount: 1,
                    },
                    method: "POST",
                    header: {
                      "Content-Type": "application/x-www-form-urlencoded"
                    },
                    success: function(res1) {
                      // console.log(that.data)
                      console.log("res1", res1)
                      if (res1.data.error_no != 0) {
                        wx.showModal({
                          title: '哎呀～',
                          content: '创建订单失败！',
                          success: function(res) {
                            if (res.confirm) {
                              console.log('用户点击确定')
                            } else if (res.cancel) {
                              console.log('用户点击取消')
                            }
                          }
                        })
                      } else if (res1.data.error_no == 0) {
                        if (res1.data.data.to_wechat_pay_money == 0) {
                          wx.showModal({
                            title: '恭喜',
                            showCancel: false,
                            content: '支付成功！',
                            success: function(res) {

                            },
                            complete: function(res) {
                              wx.reLaunch({
                                url: '../myorder/myorder'
                              })
                            }
                          })
                        } else {
                          wx.request({
                            url: getApp().globalData.server + "/API/Pay/pay_request",
                            data: {
                              code: res.code,
                              company_id: getApp().globalData.settings.company_id,
                              shop_id: getApp().globalData.settings.shop_id,
                              order_id: res1.data.data.order_id,
                              total_to_pay_money: res1.data.data.to_wechat_pay_money,
                              user_id: getApp().globalData.userInfo_detail.user_id
                            },
                            method: "POST",
                            header: {
                              "Content-Type": "application/x-www-form-urlencoded"
                            },
                            success: function(res2) {
                              console.log("res2", res2)
                              console.log("pay", res1.data.data.to_wechat_pay_money)
                              if (res2.data.error_no != 0) {
                                wx.showModal({
                                  title: '哎呀',
                                  content: '请求支付失败！' + res2.data.data.error_msg,
                                  success: function(res) {
                                    if (res.confirm) {
                                      console.log('用户点击确定')
                                    } else if (res.cancel) {
                                      console.log('用户点击取消')
                                    }

                                  }
                                })
                              } else if (res2.data.error_no == 0) {
                                var pay = require("../pay/pay.js").pay
                                pay(res2, that, 4)
                              }
                            },
                            fail: function(res) {
                              wx.showModal({
                                title: '哎呀～',
                                content: '请求支付失败！',
                                success: function(res) {
                                  if (res.confirm) {
                                    console.log('用户点击确定')
                                  } else if (res.cancel) {
                                    console.log('用户点击取消')
                                  }
                                },
                                complete: function(res) {
                                  wx.reLaunch({
                                    url: '../myorder/myorder'
                                  })
                                }

                              })
                            }
                          })
                        }
                      }
                    },
                    fail: function(res) {
                      wx.showModal({
                        title: '哎呀～',
                        content: '网络不在状态呢！',
                        success: function(res) {
                          if (res.confirm) {
                            console.log('用户点击确定')
                          } else if (res.cancel) {
                            console.log('用户点击取消')
                          }
                        }
                      })
                    }
                  })
                }
              } else {
                wx.request({
                  url: getApp().globalData.server + "/API/Shopping/online_pay",
                  data: {
                    user_id: getApp().globalData.userInfo_detail.user_id,
                    username: getApp().globalData.userInfo_detail.username,
                    total_to_pay_money: that.data.consumption,
                    balance_pay_money: that.data.balance_pay,
                    company_id: getApp().globalData.settings.company_id,
                    shop_id: getApp().globalData.settings.shop_id,
                    shop_name: getApp().globalData.settings.shop_name,
                    note: that.data.beizhu,
                    discount_card_id: that.data.discount_card_id,
                    must_dec_main_balance: 0,
                    voucher_record_id: that.data.voucher_card_id,
                  },
                  method: "POST",
                  header: {
                    "Content-Type": "application/x-www-form-urlencoded"
                  },
                  success: function(res1) {
                    // console.log(that.data)
                    console.log("res1", res1)
                    if (res1.data.error_no != 0) {
                      wx.showModal({
                        title: '哎呀～',
                        content: '创建订单失败！',
                        success: function(res) {
                          if (res.confirm) {
                            console.log('用户点击确定')
                          } else if (res.cancel) {
                            console.log('用户点击取消')
                          }
                        }
                      })
                    } else if (res1.data.error_no == 0) {
                      if (res1.data.data.to_wechat_pay_money == 0) {
                        wx.showModal({
                          title: '恭喜',
                          showCancel: false,
                          content: '支付成功！',
                          success: function(res) {

                          },
                          complete: function(res) {
                            wx.reLaunch({
                              url: '../myorder/myorder'
                            })
                          }
                        })
                      } else {
                        wx.request({
                          url: getApp().globalData.server + "/API/Pay/pay_request",
                          data: {
                            code: res.code,
                            company_id: getApp().globalData.settings.company_id,
                            shop_id: getApp().globalData.settings.shop_id,
                            order_id: res1.data.data.order_id,
                            total_to_pay_money: res1.data.data.to_wechat_pay_money,
                            user_id: getApp().globalData.userInfo_detail.user_id
                          },
                          method: "POST",
                          header: {
                            "Content-Type": "application/x-www-form-urlencoded"
                          },
                          success: function(res2) {
                            console.log("res2", res2)
                            console.log("pay", res1.data.data.to_wechat_pay_money)
                            if (res2.data.error_no != 0) {
                              wx.showModal({
                                title: '哎呀',
                                content: '请求支付失败！' + res2.data.data.error_msg,
                                success: function(res) {
                                  if (res.confirm) {
                                    console.log('用户点击确定')
                                  } else if (res.cancel) {
                                    console.log('用户点击取消')
                                  }

                                }
                              })
                            } else if (res2.data.error_no == 0) {
                              var pay = require("../pay/pay.js").pay
                              pay(res2, that, 4)
                            }
                          },
                          fail: function(res) {
                            wx.showModal({
                              title: '哎呀～',
                              content: '请求支付失败！',
                              success: function(res) {
                                if (res.confirm) {
                                  console.log('用户点击确定')
                                } else if (res.cancel) {
                                  console.log('用户点击取消')
                                }
                              },
                              complete: function(res) {
                                wx.reLaunch({
                                  url: '../myorder/myorder'
                                })
                              }

                            })
                          }
                        })
                      }
                    }
                  },
                  fail: function(res) {
                    wx.showModal({
                      title: '哎呀～',
                      content: '网络不在状态呢！',
                      success: function(res) {
                        if (res.confirm) {
                          console.log('用户点击确定')
                        } else if (res.cancel) {
                          console.log('用户点击取消')
                        }
                      }
                    })
                  }
                })
              }
            } else {
              console.log('登录失败！' + res.errMsg)
              wx.showModal({
                title: '哎呀～',
                content: '内部错误1',
                success: function(res) {
                  if (res.confirm) {
                    console.log('用户点击确定')
                  } else if (res.cancel) {
                    console.log('用户点击取消')
                  }
                }
              })
            }
          }
        });
        that.setData({
          opacity: 0.4,
          disabled: 1
        })
      }
    }
  },

  //输入备注
  beizhu_input: function(e) {
    var beizhu = e.detail.value
    var that = this
    that.setData({
      beizhu: beizhu,
      content: beizhu
    })

  },
  //输入消费金额
  money_input: function(e) {
    // e.detail.value = e.detail.value.replace(/[^\d.]/g, "")  //清除“数字”和“.”以外的字符   
    // e.detail.value = e.detail.value.replace(/\.{2,}/g, ".") //只保留第一个. 清除多余的   
    // e.detail.value = e.detail.value.replace(".", "$#$").replace(/\./g, "").replace("$#$", ".")
    // e.detail.value = e.detail.value.replace(/^(\-)*(\d+)\.(\d\d).*$/, '$1$2.$3')//只能输入两个小数 


    if (e.detail.value) {
      var money = parseFloat(e.detail.value.replace(/^(\-)*(\d+)\.(\d\d).*$/, '$1$2.$3'))
      var xiaofeijine = e.detail.value.replace(/^(\-)*(\d+)\.(\d\d).*$/, '$1$2.$3')
    } else {
      var money = 0
    }
    this.setData({
      minput: money,
    })

    console.log(xiaofeijine)
    var that = this
    var flag = that.data.flag
    //  if(e.detail.value[0]==1){
    //    money = 0
    //  }
    var money_low = 0
    var balanceofpay = that.data.balance_pay
    if (flag == 1) {
      var money_total = money
      var user_balance = that.data.now.balance

      if (money_total >= user_balance) {
        // money_low = (parseInt(money_total*100) - parseInt(user_balance*100))
        money_low = parseFloat(money_total - user_balance).toFixed(2)
        console.log(money_total, parseInt(money_total * 100), parseInt(user_balance * 100), money_low)
        balanceofpay = user_balance
      } else {
        money_low = 0
        balanceofpay = money_total
      }
    } else if (flag == 0) {
      money_low = money
      balanceofpay = 0
    }

    var abcd = 'dis.checked'
    that.setData({
      [abcd]: '',
      xiaofeijine: xiaofeijine,
      consumption: money,
      // consumption_0: money_low,  因为改为输入金额取消掉勾选改为下一行代码
      consumption_0: money,
      yuedisplay: money,
      balance_pay: balanceofpay,
      youhui: 0,
      youhuiInVoucher: 0,
      discount: 1,
      selectcard: '点我选择折扣券',
      selectcardVoucher: '点我选择代金券',
      discount_card_id: '',
      discount_temp: '',
      voucher_card_id: '',
      voucherID_temp: '',
      way: [{
          name: 'balance',
          value: 1,
          disabled: '',
          checked: false
        },
        {
          name: 'coupons',
          value: 2,
          disabled: '',
          checked: false
        },
        {
          name: 'voucher',
          value: 3,
          disabled: '',
          checked: false
        }
      ],
    })
  },
  //加载函数
  onLoad: function() {
    var that = this
    console.log('way', that.data.way[0].checked)
    that.setData({
        company_name: getApp().globalData.settings.company_name, //全局变量赋值
        shop_name: getApp().globalData.settings.shop_name,
      }),

      //根据用户id得到其账户的剩余金额
      wx.request({
        url: getApp().globalData.server + '/API/User/get_balance_by_user_id',
        data: {
          shop_id: getApp().globalData.settings.shop_id,
          company_id: getApp().globalData.settings.company_id,
          user_id: getApp().globalData.userInfo_detail.user_id,
        },
        method: "POST",
        header: {
          "Content-Type": "application/x-www-form-urlencoded"
        },
        success: function(res) {
          console.log('根据用户id得到其账户的剩余金额', res.data)
          if (res.data.error_no != 0) {
            wx.showModal({
              title: '哎呀～',
              content: '出错了呢！' + res.data.data.error_msg,
              success: function(res) {
                if (res.confirm) {
                  console.log('用户点击确定')
                } else if (res.cancel) {
                  console.log('用户点击取消')
                }
              }
            })
          } else if (res.data.error_no == 0) {
            var tem = "now.balance"
            that.setData({
              [tem]: res.data.data.total_balance
            })
            if (res.data.data.total_balance == 0) {
              var batem = "way[0].disabled"
              that.setData({
                [batem]: 'true',
              })
            }
          }
        },
        fail: function(res) {
          wx.showModal({
            title: '哎呀～',
            content: '网络不在状态呢！',
            success: function(res) {
              if (res.confirm) {
                console.log('用户点击确定')
              } else if (res.cancel) {
                console.log('用户点击取消')
              }
            }
          })
        }
      })
  },

  powerDrawer: function(e) {
    var currentStatu = e.currentTarget.dataset.statu;
    this.util(currentStatu)
    var that = this;
    //根据用户id得到其账户的折扣券
    wx.request({
      url: getApp().globalData.server + '/API/Card/get_all_discount_and_voucher_by_user_id',
      data: {
        shop_id: getApp().globalData.settings.shop_id,
        company_id: getApp().globalData.settings.company_id,
        user_id: getApp().globalData.userInfo_detail.user_id,
      },
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function(res) {
        console.log('根据用户id得到其账户的优惠券', res)
        if (res.data.error_no != 0) {
          wx.showModal({
            title: '哎呀～',
            content: '出错了呢！' + res.data.data.error_msg,
            success: function(res) {
              if (res.confirm) {
                console.log('用户点击确定')
              } else if (res.cancel) {
                console.log('用户点击取消')
              }
            }
          })
        } else if (res.data.error_no == 0) {
          var coupons_temp = res.data.data.discount_cards.has_discount_cards

          var cannot_use_cards_temp = [];
          var can_use_cards_temp = [];
          for (var i in coupons_temp) {
            if (coupons_temp[i].at_least_consume_money > that.data.consumption) {
              cannot_use_cards_temp.push(coupons_temp[i])
            } else {
              can_use_cards_temp.push(coupons_temp[i])
            }
          }
          console.log('可用的优惠券', can_use_cards_temp)
          console.log('不可用的优惠券', cannot_use_cards_temp)
          that.setData({
            can_use_cards: can_use_cards_temp,
            cannot_use_cards: cannot_use_cards_temp
          })

        }
      },
      fail: function(res) {
        wx.showModal({
          title: '哎呀～',
          content: '网络不在状态呢！',
          success: function(res) {
            if (res.confirm) {
              console.log('用户点击确定')
            } else if (res.cancel) {
              console.log('用户点击取消')
            }
          }
        })
      }
    })
  },

  powerDrawerVoucher: function(e) { //代金券部分
    var currentStatuVoucher = e.currentTarget.dataset.statu;
    this.utilVoucher(currentStatuVoucher)
    var that = this;
    //根据用户id得到其账户的代金券
    wx.request({
      url: getApp().globalData.server + '/API/Card/get_all_discount_and_voucher_by_user_id',
      data: {
        shop_id: getApp().globalData.settings.shop_id,
        company_id: getApp().globalData.settings.company_id,
        user_id: getApp().globalData.userInfo_detail.user_id,
      },
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function(res) {
        console.log('根据用户id得到其账户的代金券', res)
        if (res.data.error_no != 0) {
          wx.showModal({
            title: '哎呀～',
            content: '出错了呢！' + res.data.data.error_msg,
            success: function(res) {
              if (res.confirm) {
                console.log('用户点击确定')
              } else if (res.cancel) {
                console.log('用户点击取消')
              }
            }
          })
        } else if (res.data.error_no == 0) {
          var voucher_temp = res.data.data.cash.has_cash
          var cannot_use_cards_temp = [];
          var can_use_cards_temp = [];
          console.log("voucher_temp", voucher_temp)
          for (var i in voucher_temp) {
            // console.log(voucher_temp[i].at_least_consume_money > that.data.consumption)
            if (voucher_temp[i].at_least_consume_money > that.data.consumption) {
              cannot_use_cards_temp.push(voucher_temp[i])
            } else {
              can_use_cards_temp.push(voucher_temp[i])
            }
          }
          console.log('可用的优惠券', can_use_cards_temp)
          console.log('不可用的优惠券', cannot_use_cards_temp)
          that.setData({
            can_use_cards: can_use_cards_temp,
            cannot_use_cards: cannot_use_cards_temp
          })

        }
      },
      fail: function(res) {
        wx.showModal({
          title: '哎呀～',
          content: '网络不在状态呢！',
          success: function(res) {
            if (res.confirm) {
              console.log('用户点击确定')
            } else if (res.cancel) {
              console.log('用户点击取消')
            }
          }
        })
      }
    })
  },

  util: function(currentStatu) {
    /* 动画部分 */
    // 第1步：创建动画实例   
    var animation = wx.createAnimation({
      duration: 200, //动画时长  
      timingFunction: "linear", //线性  
      delay: 0 //0则不延迟  
    });

    // 第2步：这个动画实例赋给当前的动画实例  
    this.animation = animation;

    // 第3步：执行第一组动画：Y轴偏移240px后(盒子高度是240px)，停  
    animation.translateY(240).step();

    // 第4步：导出动画对象赋给数据对象储存  
    this.setData({
      animationData: animation.export()
    })

    // 第5步：设置定时器到指定时候后，执行第二组动画  
    setTimeout(function() {
      // 执行第二组动画：Y轴不偏移，停  
      animation.translateY(0).step()
      // 给数据对象储存的第一组动画，更替为执行完第二组动画的动画对象  
      this.setData({
        animationData: animation
      })

      //关闭抽屉  
      if (currentStatu == "close") {
        this.setData({
          showModalStatus: false
        });
      }
    }.bind(this), 200)

    // 显示抽屉  
    if (currentStatu == "open") {
      this.setData({
        showModalStatus: true
      });
    }
  },
  utilVoucher: function(currentStatu) {
    /* 动画部分 */
    // 第1步：创建动画实例   
    var animation = wx.createAnimation({
      duration: 200, //动画时长  
      timingFunction: "linear", //线性  
      delay: 0 //0则不延迟  
    });

    // 第2步：这个动画实例赋给当前的动画实例  
    this.animation = animation;

    // 第3步：执行第一组动画：Y轴偏移240px后(盒子高度是240px)，停  
    animation.translateY(240).step();

    // 第4步：导出动画对象赋给数据对象储存  
    this.setData({
      animationData: animation.export()
    })

    // 第5步：设置定时器到指定时候后，执行第二组动画  
    setTimeout(function() {
      // 执行第二组动画：Y轴不偏移，停  
      animation.translateY(0).step()
      // 给数据对象储存的第一组动画，更替为执行完第二组动画的动画对象  
      this.setData({
        animationData: animation
      })

      //关闭抽屉  
      if (currentStatu == "close") {
        this.setData({
          showModalStatusVoucher: false
        });
      }
    }.bind(this), 200)

    // 显示抽屉  
    if (currentStatu == "open") {
      this.setData({
        showModalStatusVoucher: true
      });
    }
  },

  //点击选择折扣券按钮
  onTabsItemTap: function(event) {
    if (this.data.isdisc == false) {
      var that = this;
      var currentStatu = "close";
      var cardchange = that.data.consumption;
      var balanceofpay = that.data.balance_pay;
      this.util(currentStatu)
      var index = event.currentTarget.dataset['index'];
      var cardId_temp = that.data.discount_card_id;
      var youhuidisplay = 0;
      var way_temp = that.data.way;
      var e = {
        detail: {
          value: []
        }
      };
      if (that.data.boxstate == 2) { //状态2优惠券+微信支付
        cardchange = (that.data.consumption * that.data.can_use_cards[index].discount / 10).toFixed(2)
        cardId_temp = that.data.can_use_cards[index].id
        youhuidisplay = (that.data.consumption - (that.data.consumption * that.data.can_use_cards[index].discount / 10).toFixed(2)).toFixed(2)
        way_temp[2].checked = false;
        e.detail.value.length = 1
        e.detail.value[0] = "2"
      } else if (that.data.boxstate == 3) { //状态3 优惠券+余额支付
        e.detail.value.length = 2
        way_temp[2].checked = false;

        var money_total = (that.data.consumption * that.data.can_use_cards[index].discount / 10).toFixed(2)
        var user_balance = that.data.now.balance
        cardId_temp = that.data.can_use_cards[index].id
        youhuidisplay = (that.data.consumption - (that.data.consumption * that.data.can_use_cards[index].discount / 10).toFixed(2)).toFixed(2)
        if (that.data.balance_pay != undefined) {
          balanceofpay = that.data.balance_pay
        }

        // console.log(that.data.balance_pay)
        if (money_total >= user_balance) {
          cardchange = parseFloat(money_total - user_balance).toFixed(2)
          balanceofpay = user_balance
        } else {
          cardchange = 0
          balanceofpay = money_total
        }

      } else {
        youhuidisplay = (that.data.consumption - (that.data.consumption * that.data.can_use_cards[index].discount / 10).toFixed(2)).toFixed(2)
        way_temp[1].checked = true
        way_temp[2].checked = false;
        if (way_temp[0].checked == true) e.detail.value.length = 2
        else {
          e.detail.value.length = 1
          e.detail.value[0] = "2"
        }
      }
      this.setData({
        selectcard: this.data.can_use_cards[index].discount_card_name,
        discount: that.data.can_use_cards[index].discount / 10,
        discount_card_id: cardId_temp,
        discountID_temp: that.data.can_use_cards[index].id,
        consumption_after_discount: (that.data.consumption * that.data.can_use_cards[index].discount / 10).toFixed(2),
        youhui: (that.data.consumption - (that.data.consumption * that.data.can_use_cards[index].discount / 10).toFixed(2)).toFixed(2),
        consumption_0: cardchange,
        balance_pay: balanceofpay,
        yuedisplay: (that.data.consumption - youhuidisplay).toFixed(2),
        show: false,
        way: way_temp
        // selectcard_id: this.data.now.has_times_cards[index].id,
      })


      console.log(e)
      this.listenCheckboxChange(e)
      // console.log('现在的折扣',that.data.discount)
      // if (this.data.now.has_times_cards[index].rest_times == 0) {
      //   var tem1 = "way[1].checked"
      //   var tem2 = "way[1].disabled"
      //   that.setData({
      //     [tem1]: '',
      //     [tem2]: 'true',
      //   })
      // }
      // else {
      //   var tem1 = "way[1].disabled"
      //   var tem2 = "way[1].checked"
      //   var tem3 = "way[0].checked"
      //   var tem4 = "way[2].checked"
      //   that.setData({
      //     [tem1]: '',
      //     [tem2]: 'true',
      //     [tem3]: '',
      //     [tem4]: '',
      //     selectway: 2
      //   })
      // }

    }
  },

  //点击选择代金券按钮
  onTabsItemTapVoucher: function(event) {
    if (this.data.isdisc == false) {
      var that = this;
      var currentStatuVoucher = "close";
      var cardchange = that.data.consumption;
      var balanceofpay = that.data.balance_pay;
      this.utilVoucher(currentStatuVoucher)
      var index = event.currentTarget.dataset['index'];
      var cardId_temp = that.data.voucher_card_id;
      // var youhuidisplay = 0;
      var way_temp = that.data.way;
      var e = {
        detail: {
          value: []
        }
      };

      var youhuidisplayInVoucher = 0
      console.log(that.data.boxstate)
      if (that.data.boxstate == 4) { //状态4 代金券+微信支付
        cardchange = (that.data.consumption - that.data.can_use_cards[index].discount_amount).toFixed(2)
        console.log(cardchange)
        cardId_temp = that.data.can_use_cards[index].id
        // youhuidisplay = (that.data.consumption - (that.data.consumption * that.data.can_use_cards[index].discount / 10).toFixed(2)).toFixed(2)
        way_temp[1].checked = false;
        youhuidisplayInVoucher = that.data.can_use_cards[index].discount_amount
        e.detail.value.length = 1
        e.detail.value[0] = "3"
      } else if (that.data.boxstate == 5) { //状态5 代金券+余额支付
        e.detail.value.length = 2
        way_temp[1].checked = false;

        var money_total = (that.data.consumption - that.data.can_use_cards[index].discount_amount).toFixed(2)
        console.log(cardchange)
        var user_balance = that.data.now.balance
        cardId_temp = that.data.can_use_cards[index].id
        // youhuidisplay = (that.data.consumption - (that.data.consumption * that.data.can_use_cards[index].discount / 10).toFixed(2)).toFixed(2)
        youhuidisplayInVoucher = that.data.can_use_cards[index].discount_amount
        if (that.data.balance_pay != undefined) {
          balanceofpay = that.data.balance_pay
        }

        // console.log(that.data.balance_pay)
        if (money_total >= user_balance) {
          cardchange = parseFloat(money_total - user_balance).toFixed(2)
          balanceofpay = user_balance
        } else {
          cardchange = 0
          balanceofpay = money_total
        }

      } else {
        youhuidisplayInVoucher = that.data.can_use_cards[index].discount_amount
        cardchange = (that.data.consumption - that.data.can_use_cards[index].discount_amount).toFixed(2)
        cardId_temp = that.data.can_use_cards[index].id
        way_temp[2].checked = true
        way_temp[1].checked = false;
        if (way_temp[0].checked == true) e.detail.value.length = 2
        else {
          e.detail.value.length = 1
          e.detail.value[0] = "3"
        }
      }
      cardchange = cardchange < 0 ? 0 : cardchange
      console.log(cardchange)
      console.log(cardId_temp)
      this.setData({
        selectcardVoucher: this.data.can_use_cards[index].voucher_name,
        discountVoucher: that.data.can_use_cards[index].discount_amount,
        voucher_card_id: cardId_temp,
        voucherID_temp: that.data.can_use_cards[index].id,
        consumption_after_discount: (that.data.consumption - that.data.can_use_cards[index].discount_amount).toFixed(2),
        // youhui: (that.data.consumption - (that.data.consumption * that.data.can_use_cards[index].discount / 10).toFixed(2)).toFixed(2),
        // 代金券的操作中不会操作和折扣券有关的东西
        youhuiInVoucher: youhuidisplayInVoucher,
        consumption_0: cardchange,
        balance_pay: balanceofpay,
        yuedisplay: (that.data.consumption - youhuidisplayInVoucher).toFixed(2) < 0 ? 0 : (that.data.consumption - youhuidisplayInVoucher).toFixed(2),
        show: false,
        way: way_temp
      })
      console.log(that.data.voucher_card_id)

      console.log(e)
      // this.listenCheckboxChange(e)
      // console.log('现在的折扣',that.data.discount)
      // if (this.data.now.has_times_cards[index].rest_times == 0) {
      //   var tem1 = "way[1].checked"
      //   var tem2 = "way[1].disabled"
      //   that.setData({
      //     [tem1]: '',
      //     [tem2]: 'true',
      //   })
      // }
      // else {
      //   var tem1 = "way[1].disabled"
      //   var tem2 = "way[1].checked"
      //   var tem3 = "way[0].checked"
      //   var tem4 = "way[2].checked"
      //   that.setData({
      //     [tem1]: '',
      //     [tem2]: 'true',
      //     [tem3]: '',
      //     [tem4]: '',
      //     selectway: 2
      //   })
      // }
    }
  },
  // ?/？？？？
  // 监听box改变
  listenCheckboxChange: function(e) {
    var that = this;
    console.log(e)
    var discount_temp = that.data.discount
    var money = 0
    var flag = 1
    var boxstate = 0 //盒子选中状态
    var discountID = '0'
    var voucherID = '0'
    var balanceofpay = 0
    var youhuidisplay = 0
    var youhuidisplayInVoucher = 0
    var way_temp = that.data.way;
    console.log("way", that.data.way)
    if (e.detail.value.length == 1 && e.detail.value[0] == "1") {
      // if (e.detail.value.length == 1) {
      var money_total = that.data.consumption
      var user_balance = that.data.now.balance
      boxstate = 1 //状态1余额支付
      way_temp[0].checked = true
      way_temp[1].checked = false
      way_temp[2].checked = false
      youhuidisplay = 0
      if (that.data.balance_pay != undefined) {
        balanceofpay = that.data.balance_pay
      }

      // console.log(that.data.balance_pay)
      if (money_total >= user_balance) {
        money = parseFloat(money_total - user_balance).toFixed(2)
        balanceofpay = user_balance
      } else {
        money = 0
        balanceofpay = money_total
      }

    } else if (e.detail.value.length == 0) {
      console.log(e.detail.value)
      boxstate = 0 //状态0微信支付
      youhuidisplay = 0
      money = that.data.consumption
      balanceofpay = 0
      flag = 0
      way_temp[0].checked = false
      way_temp[1].checked = false
      way_temp[2].checked = false
    } else if ((e.detail.value.length == 1 && e.detail.value[0] == "2") || ((e.detail.value.length == 2 && e.detail.value[0] == "3") && e.detail.value[1] == "2")) {
      boxstate = 2 //状态2 折扣券支付
      way_temp[0].checked = false
      way_temp[1].checked = true
      way_temp[2].checked = false
      youhuidisplay = (that.data.consumption - (that.data.consumption * that.data.discount).toFixed(2)).toFixed(2)
      money = (that.data.consumption - that.data.youhui).toFixed(2)
      balanceofpay = 0
      flag = 0
      discountID = that.data.discountID_temp
      voucherID = '0'
    } else if ((e.detail.value.length == 1 && e.detail.value[0] == "3") || (e.detail.value.length == 2 && e.detail.value[0] == "2" && e.detail.value[1] == "3")) {
      boxstate = 4 //状态4 代金券支付
      way_temp[0].checked = false
      way_temp[1].checked = false
      way_temp[2].checked = true
      youhuidisplay = that.data.discountVoucher
      //  youhuidisplayInVoucher = 
      money = (that.data.consumption - that.data.youhuiInVoucher).toFixed(2) < 0 ? 0 : (that.data.consumption - that.data.youhuiInVoucher).toFixed(2)
      balanceofpay = 0
      flag = 0
      discountID = '0'
      voucherID = that.data.voucherID_temp
    } else if ((e.detail.value.length == 2 && (e.detail.value[0] == 2 || e.detail.value[1] == 2)) || (e.detail.value.length == 3 && e.detail.value[2] == 2)) {
      boxstate = 3 //状态3 折扣券+余额支付
      way_temp[0].checked = true
      way_temp[1].checked = true
      way_temp[2].checked = false
      youhuidisplay = (that.data.consumption - (that.data.consumption * that.data.discount).toFixed(2)).toFixed(2)
      // if (e.detail.value.length == 1) {
      var money_total = that.data.consumption - that.data.youhui
      var user_balance = that.data.now.balance
      discountID = that.data.discountID_temp
      voucherID = '0'
      if (that.data.balance_pay != undefined) {
        balanceofpay = that.data.balance_pay
      }

      // console.log(that.data.balance_pay)
      if (money_total >= user_balance) {
        money = parseFloat(money_total - user_balance).toFixed(2)
        balanceofpay = user_balance
      } else {
        money = 0
        balanceofpay = money_total
      }

    } else if ((e.detail.value.length == 2 && (e.detail.value[0] == 3 || e.detail.value[1] == 3)) || (e.detail.value.length == 3 && e.detail.value[2] == 3)) {
      boxstate = 5 //状态5 代金券+余额支付
      way_temp[0].checked = true
      way_temp[1].checked = false
      way_temp[2].checked = true
      youhuidisplay = that.data.discountVoucher
      // if (e.detail.value.length == 1) {
      var money_total = that.data.consumption - that.data.youhuiInVoucher
      money_total = money_total < 0 ? 0 : money_total
      var user_balance = that.data.now.balance
      discountID = '0'
      voucherID = that.data.voucherID_temp
      if (that.data.balance_pay != undefined) {
        balanceofpay = that.data.balance_pay
      }

      // console.log(that.data.balance_pay)
      if (money_total >= user_balance) {
        money = parseFloat(money_total - user_balance).toFixed(2)
        balanceofpay = user_balance
      } else {
        money = 0
        balanceofpay = money_total
      }

    }

    if (that.data.isdisc == true) {
      that.setData({
        boxstate: boxstate,
        consumption_0: (money * that.data.discount_weekday.discount).toFixed(2),
        flag: flag,
        balance_pay: balanceofpay,
        yuedisplay: (that.data.consumption * that.data.discount_weekday.discount - youhuidisplay).toFixed(2) < 0 ? 0 : (that.data.consumption * that.data.discount_weekday.discount - youhuidisplay).toFixed(2),
        discount_card_id: discountID,
        voucher_card_id: voucherID,
        way: way_temp
      })
    } else {
      that.setData({
        boxstate: boxstate,
        consumption_0: money,
        flag: flag,
        balance_pay: balanceofpay,
        yuedisplay: (that.data.consumption - youhuidisplay).toFixed(2) < 0 ? 0 : (that.data.consumption - youhuidisplay).toFixed(2),
        discount_card_id: discountID,
        voucher_card_id: voucherID,
        way: way_temp
      })
    }
    // var tem = 20
    // console.log(data)


  },
  recharge: function() {
    wx.navigateTo({
      url: '/pages/recharge/recharge'
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    var that = this

    //得到指定店铺的详细信息
    wx.request({
      url: getApp().globalData.server + '/API/Shop/get_one_shop_discount',
      data: {
        shop_id: getApp().globalData.settings.shop_id,
        company_id: getApp().globalData.settings.company_id,
      },
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function(res) {
        console.log('得到指定店铺的详细信息', res.data)
        if (res.data.error_no != 0) {
          wx.showModal({
            title: '哎呀～',
            content: '出错了呢！' + res.data.data.error_msg,
            success: function(res) {
              if (res.confirm) {
                console.log('用户点击确定')
              } else if (res.cancel) {
                console.log('用户点击取消')
              }
            }
          })
        } else if (res.data.error_no == 0) {
          that.setData({
            discount_weekday: res.data.data.all_shops.discount_weekday
          })
        }
      },
      fail: function(res) {
        wx.showModal({
          title: '哎呀～',
          content: '网络不在状态呢！',
          success: function(res) {
            if (res.confirm) {
              console.log('用户点击确定')
            } else if (res.cancel) {
              console.log('用户点击取消')
            }
          }
        })
      }
    })
  },
})
// pages/pay_complete/pay_complete.js
var util = require('../../utils/util.js');

Page({

  /**
   * 页面的初始数据
   */
  data: {
    price: 0,
    windowHeight: getApp().globalData.windowHeight,
    reminder: 5,
    t: 0,
    source: 0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this
    console.log(options)
    this.setData({
      price: options.price,
    })

    if (options.source == "3") {
      that.setData({
        source: options.source,
      })
    }
  },


  timer: function () {
    var that = this
    var old = that.data.reminder
    that.setData({
      reminder: old - 1
    })
    if (that.data.reminder == 0) {
      that.jump()
    }
  },

  jump: function () {
    var that = this
    if (that.data.source == "3") {
      wx.redirectTo({
        url: '../myorder/myorder?tab=' + 1,
      })
    } else {
      wx.redirectTo({
        url: '../my_list/my_list?pay=1',
      })
    }

  },

  complete: function () {
    var that = this
    clearInterval(that.data.t)
    this.jump()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    var that = this
    var t = setInterval(that.timer, 1000)
    // console.log(t)
    that.setData({
      t: t
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return (util.share())
  }
})
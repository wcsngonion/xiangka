// pages/payment/payment.js
var util = require('../../utils/util.js');
var app = getApp()

Page({

  /**
   * 页面的初始数据
   */
  data: {
    good: {},
    shop: "",
    quantity: 0,
    order_id: "",

    shop_name: 'Getting Data...',
    goods_id: '',
    is_use_payment: '0',

    //滚动条相关设置
    text: '',
    marqueePace: 0.6,//滚动速度
    marqueeDistance: 0,//初始滚动距离
    marquee2copy_status: false,
    //marquee2_margin: 200,//循环首尾间距px
    size: 35,//字体大小rpx
    orientation: 'left',//滚动方向
    interval: 20,// 时间间隔

    //数据信息
    goods: {
      detail: {
        name: "商品名称",
        img_url: "/images/goo.png",
        brief_description: "关于本商品的描述",
        unit_price: 1.00,
      },
      num: 1,
    },
    need: {
      balance: "5.00",
      times: 1,
      wechatpay: "5.00",
    },

    //当前账户信息
    now: {
      balance: 10,
      has_times_cards: [
        { card_name: '白金VIP卡', rest_times: 3, card_id: 1 },
        { card_name: '黄金VIP卡', rest_times: 0, card_id: 2 },
        { card_name: '钻石VIP卡', rest_times: 0, card_id: 3 },
      ],
      no_times_cards: [
        { card_name: '白金VIP卡', rest_times: 3, card_id: 1 },
        { card_name: '黄金VIP卡', rest_times: 0, card_id: 2 },
        { card_name: '钻石VIP卡', rest_times: 0, card_id: 3 },
      ],
    },
    selectcard: '',
    selectcard_id: '',

    //付款方式信息
    way: [
      { name: 'balance', value: 1, disabled: '', checked: '' },
      { name: 'times', value: 2, disabled: '', checked: '' },
      { name: 'wechatpay', value: 3, disabled: '', checked: '' }
    ],
    selectway: undefined,

    //支付过程
    total_to_pay_money: 0,
    order_id: '',
    has_dec_main_balance: '',
    has_dec_second_balance: '',

    showModalStatus: false,
  },

  goback: function () {
    wx.redirectTo({
      url: '/pages/index/index'
    })
  },

  recharge: function () {
    wx.navigateTo({
      url: '/pages/recharge/recharge'
    })
  },

  paymoney: function () {
    var that = this
    console.log("selectcard_id", this.data.selectcard_id)
    if (that.data.selectway == undefined) {
      wx.showModal({
        title: '提示',
        showCancel: false,
        content: '请选择一种支付方式',
        success: function (res) { }
      })
    } else if (that.data.selectway == "1") {
      //扣除指定用户的指定余额
      wx.request({
        url: getApp().globalData.server + '/API/User/dec_balance',
        data: {
          shop_id: getApp().globalData.settings.shop_id,
          company_id: getApp().globalData.settings.company_id,
          user_id: getApp().globalData.userInfo_detail.user_id,
          to_dec_money: that.data.quantity * that.data.good.exchange_cost_money,
          is_need_confirm_order : 0,
          order_id: that.data.order_id
        },
        method: "POST",
        header: {
          "Content-Type": "application/x-www-form-urlencoded"
        },
        success: function (res) {
          console.log('扣除指定用户的指定余额:', res.data)
          if (res.data.error_no != 0) {
            wx.showModal({
              title: '哎呀～',
              content: '出错了呢！' + res.data.data.error_msg,
              success: function (res) {
                if (res.confirm) {
                  console.log('用户点击确定')
                } else if (res.cancel) {
                  console.log('用户点击取消')
                }
              }
            })
          }
          else if (res.data.error_no == 0) {
            if (res.data.data.need_to_pay_wechat_money == 0){
              wx.showModal({
                title: ' 提示',
                content: '支付成功！' ,
                success: function (res) {
                  if (res.confirm) {
                    console.log('用户点击确定')
                    wx.redirectTo({
                      url: '../myorder/myorder?tab=' + 1,
                    })
                  } else if (res.cancel) {
                    console.log('用户点击取消')
                    wx.redirectTo({
                      url: '../myorder/myorder?tab=' + 1,
                    })
                  }
                }
              })
            } else if (res.data.data.need_to_pay_wechat_money != 0){
              var need_to_pay_wechat_money = res.data.data.need_to_pay_wechat_money
              wx.login({
                success: function (res) {
                  if (res.code) {
                    //请求支付
                    wx.request({
                      url: getApp().globalData.server + "/API/Pay/pay_request",
                      data: {
                        code: res.code,
                        company_id: getApp().globalData.settings.company_id,
                        shop_id: getApp().globalData.settings.shop_id,
                        order_id: that.data.order_id,
                        total_to_pay_money: need_to_pay_wechat_money,
                        user_id: getApp().globalData.userInfo_detail.user_id
                      },
                      method: "POST",
                      header: {
                        "Content-Type": "application/x-www-form-urlencoded"
                      },
                      success: function (res2) {
                        console.log(res2)
                        if (res2.data.error_no != 0) {
                          wx.showModal({
                            title: '哎呀',
                            content: '请求支付失败！' + res2.data.data.error_msg,
                            success: function (res) {
                              if (res.confirm) {
                                console.log('用户点击确定')
                              } else if (res.cancel) {
                                console.log('用户点击取消')
                              }
                            }
                          })
                        }
                        else if (res2.data.error_no == 0) {
                          var pay = require("../pay/pay.js").pay
                          pay(res2, that, 3, need_to_pay_wechat_money)
                        }
                      },
                      fail: function (res) {
                        wx.showModal({
                          title: '哎呀～',
                          content: '请求支付失败！',
                          success: function (res) {
                            if (res.confirm) {
                              console.log('用户点击确定')
                            } else if (res.cancel) {
                              console.log('用户点击取消')
                            }
                          }
                        })
                      }
                    })
                  } else {
                    console.log('登录失败！' + res.errMsg)
                  }
                }
              });
            }
          }
        },
        fail: function (res) {
          wx.showModal({
            title: '哎呀～',
            content: '网络不在状态呢！',
            success: function (res) {
              if (res.confirm) {
                console.log('用户点击确定')
              } else if (res.cancel) {
                console.log('用户点击取消')
              }
            }
          })
        }
      })
    } else if (that.data.selectway == "3") {
      wx.login({
        success: function (res) {
          if (res.code) {
            //请求支付
            wx.request({
              url: getApp().globalData.server + "/API/Pay/pay_request",
              data: {
                code: res.code,
                company_id: getApp().globalData.settings.company_id,
                shop_id: getApp().globalData.settings.shop_id,
                order_id: that.data.order_id,
                total_to_pay_money: that.data.quantity * that.data.good.exchange_cost_money,
                user_id: getApp().globalData.userInfo_detail.user_id
              },
              method: "POST",
              header: {
                "Content-Type": "application/x-www-form-urlencoded"
              },
              success: function (res2) {
                console.log(res2)
                if (res2.data.error_no != 0) {
                  wx.showModal({
                    title: '哎呀',
                    content: '请求支付失败！' + res2.data.data.error_msg,
                    success: function (res) {
                      if (res.confirm) {
                        console.log('用户点击确定')
                      } else if (res.cancel) {
                        console.log('用户点击取消')
                      }
                    }
                  })
                }
                else if (res2.data.error_no == 0) {
                  var pay = require("../pay/pay.js").pay
                  pay(res2, that, 3, that.data.quantity * that.data.good.exchange_cost_money)
                }
              },
              fail: function (res) {
                wx.showModal({
                  title: '哎呀～',
                  content: '请求支付失败！',
                  success: function (res) {
                    if (res.confirm) {
                      console.log('用户点击确定')
                    } else if (res.cancel) {
                      console.log('用户点击取消')
                    }
                  }
                })
              }
            })
          } else {
            console.log('登录失败！' + res.errMsg)
          }
        }
      });
    }
  },

  onTabsItemTap: function (event) {
    var that = this;
    var currentStatu = "close";
    this.util(currentStatu)
    var index = event.currentTarget.dataset['index'];
    this.setData({
      selectcard: this.data.now.has_times_cards[index].card_name + " 剩余" + this.data.now.has_times_cards[index].rest_times + "次",
      selectcard_id: this.data.now.has_times_cards[index].id,
    })
    if (this.data.now.has_times_cards[index].rest_times == 0) {
      var tem1 = "way[1].checked"
      var tem2 = "way[1].disabled"
      that.setData({
        [tem1]: '',
        [tem2]: 'true',
      })
    }
    else {
      var tem1 = "way[1].disabled"
      var tem2 = "way[1].checked"
      var tem3 = "way[0].checked"
      var tem4 = "way[2].checked"
      that.setData({
        [tem1]: '',
        [tem2]: 'true',
        [tem3]: '',
        [tem4]: '',
        selectway: 2
      })
    }
  },

  powerDrawer: function (e) {
    var currentStatu = e.currentTarget.dataset.statu;
    this.util(currentStatu)
  },

  util: function (currentStatu) {
    /* 动画部分 */
    // 第1步：创建动画实例   
    var animation = wx.createAnimation({
      duration: 200,  //动画时长  
      timingFunction: "linear", //线性  
      delay: 0  //0则不延迟  
    });

    // 第2步：这个动画实例赋给当前的动画实例  
    this.animation = animation;

    // 第3步：执行第一组动画：Y轴偏移240px后(盒子高度是240px)，停  
    animation.translateY(240).step();

    // 第4步：导出动画对象赋给数据对象储存  
    this.setData({
      animationData: animation.export()
    })

    // 第5步：设置定时器到指定时候后，执行第二组动画  
    setTimeout(function () {
      // 执行第二组动画：Y轴不偏移，停  
      animation.translateY(0).step()
      // 给数据对象储存的第一组动画，更替为执行完第二组动画的动画对象  
      this.setData({
        animationData: animation
      })

      //关闭抽屉  
      if (currentStatu == "close") {
        this.setData(
          {
            showModalStatus: false
          }
        );
      }
    }.bind(this), 200)

    // 显示抽屉  
    if (currentStatu == "open") {
      this.setData(
        {
          showModalStatus: true
        }
      );
    }
  },

  /**
   * 监听checkbox事件
   */
  listenCheckboxChange: function (e) {
    var that = this;
    console.log(e)
    if (e.detail.value.length >= 2) {
      var tem = "way[" + [e.detail.value[0] - 1] + "].checked"
      that.setData({
        [tem]: '',
      })
      console.log('checkbox发生change事件，携带value值为：', e.detail.value[1])
      that.setData({
        selectway: e.detail.value[1],
      })
    }
    else {
      console.log('checkbox发生change事件，携带value值为：', e.detail.value[0])
      that.setData({
        selectway: e.detail.value[0],
      })
    }
  },

  run: function () {
    var vm = this;
    var interval = setInterval(function () {
      if (-vm.data.marqueeDistance < vm.data.length) {
        vm.setData({
          marqueeDistance: vm.data.marqueeDistance - vm.data.marqueePace,
        });
      } else {
        clearInterval(interval);
        vm.setData({
          marqueeDistance: vm.data.windowWidth
        });
        vm.run();
      }
    }, vm.data.interval);
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log("页面跳转传递", options)
    this.setData({
      shop_name: getApp().globalData.settings.shop_name,
      is_use_payment: getApp().globalData.settings.is_use_payment,
    })

    var that = this

    if (options.source == "10") {
      that.setData({
        good: JSON.parse(options.good),
        quantity: options.quantity,
        shop: getApp().globalData.settings.shop_name,
        order_id: options.order_id
      })
    }

    //根据用户id得到其账户的剩余金额
    wx.request({
      url: getApp().globalData.server + '/API/User/get_balance_by_user_id',
      data: {
        shop_id: getApp().globalData.settings.shop_id,
        company_id: getApp().globalData.settings.company_id,
        user_id: getApp().globalData.userInfo_detail.user_id,
      },
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function (res) {
        console.log('根据用户id得到其账户的剩余金额', res.data)
        if (res.data.error_no != 0) {
          wx.showModal({
            title: '哎呀～',
            content: '出错了呢！' + res.data.data.error_msg,
            success: function (res) {
              if (res.confirm) {
                console.log('用户点击确定')
              } else if (res.cancel) {
                console.log('用户点击取消')
              }
            }
          })
        }
        else if (res.data.error_no == 0) {
          var tem = "now.balance"
          that.setData({
            [tem]: res.data.data.total_balance
          })
          if (res.data.data.total_balance == 0) {
            var batem = "way[0].disabled"
            that.setData({
              [batem]: 'true',
            })
          }
        }
      },
      fail: function (res) {
        wx.showModal({
          title: '哎呀～',
          content: '网络不在状态呢！',
          success: function (res) {
            if (res.confirm) {
              console.log('用户点击确定')
            } else if (res.cancel) {
              console.log('用户点击取消')
            }
          }
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    this.setData({
      is_use_payment: getApp().globalData.settings.is_use_payment,
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    // 页面显示
    var vm = this;
    var length = vm.data.text.length * vm.data.size;//文字长度
    var windowWidth = wx.getSystemInfoSync().windowWidth;// 屏幕宽度
    vm.setData({
      length: length,
      windowWidth: windowWidth,
      marquee2_margin: length < windowWidth ? windowWidth - length : vm.data.marquee2_margin//当文字长度小于屏幕长度时，需要增加补白
    });
    vm.run();// 第一个字消失后立即从右边出现

    vm.setData({
      text: '来果果爱，就知道果果怎么爱你的....',
      is_use_payment: getApp().globalData.settings.is_use_payment,
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    return (util.share())
  }
})
// pages/mine/mine.js
var util = require('../../utils/util.js');

Page({

  /**
   * 页面的初始数据
   */
  data: {
    username: "会员卡", //用户名称
    userphoto: getApp().globalData.userInfo.avatarUrl, //用户头像
    usertel: "暂未填写", //用户电话
    balance: "0.00", //余额
    vouchers: "0", //卡券
    scores: "0", //积分
    userInfo: [],
    logincode: "",
    select_tab: 3,
    balance: 0,
    is_phone_login: false,
    is_show: true,

    //scancode
    scene: "",
    strs: "",
  },

  toShop: function() {
    // wx.showModal({
    //   title: '提示',
    //   content: '即将上线～',
    //   showCancel: false,
    //   success: function (res) {
    //     if (res.confirm) {
    //       console.log('用户点击确定')
    //     } else if (res.cancel) {
    //       console.log('用户点击取消')
    //     }
    //   },
    // })

    wx.navigateTo({
      url: '../score_shop/score_shop',
    })
  },

  buy: function() {
    wx.navigateTo({
      url: '/pages/recharge/recharge',
    })
  },

  toMyList: function() {
    wx.navigateTo({
      url: '../my_list/my_list',
    })
  },

  toMyorder: function() {
    wx.navigateTo({
      url: '../myorder/myorder',
    })
  },

  toInvite: function() {
    wx.navigateTo({
      url: '../sharing/sharing',
    })
  },

  toLottery: function() {
    wx.navigateTo({
      url: '../lottery/lottery',
    })
    // wx.showModal({
    //   title: '提示',
    //   content: '即将上线～',
    //   showCancel: false,
    //   success: function (res) {
    //     if (res.confirm) {
    //       console.log('用户点击确定')
    //     } else if (res.cancel) {
    //       console.log('用户点击取消')
    //     }
    //   }
    // })
  },

  toClockRule: function() {
    // wx.navigateTo({
    //   url: '../clock_rule/clock_rule',
    // })
    wx.showModal({
      title: '提示',
      content: '即将上线～',
      showCancel: false,
      success: function(res) {
        if (res.confirm) {
          console.log('用户点击确定')
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    if (getApp().globalData.settings.is_on_time_pay == '1') {
      this.setData({
        is_show: false
      })
    }
    if (getApp().globalData.userInfo_detail.is_phone_login == 1) {
      this.setData({
        is_phone_login: true
      })
    }
    this.setData({
      userphoto: getApp().globalData.userInfo.avatarUrl,
      vouchers: getApp().globalData.userInfo_detail.total_cards,
      scores: getApp().globalData.userInfo_detail.point,
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {
    if (getApp().globalData.userInfo_detail.is_phone_login == 1) {
      this.setData({
        is_phone_login: true
      })
    }
    this.setData({
      userphoto: getApp().globalData.userInfo.avatarUrl,
      vouchers: getApp().globalData.userInfo_detail.total_cards,
      scores: getApp().globalData.userInfo_detail.point,
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    var that = this
    this.setData({
      vouchers: getApp().globalData.userInfo_detail.total_cards,
      scores: getApp().globalData.userInfo_detail.point,
    })
    if (getApp().globalData.userInfo != "") {
      this.setData({
        userInfo: getApp().globalData.userInfo,
        userphoto: getApp().globalData.userInfo.avatarUrl
      })
    } else {
      this.setData({
        userphoto: "/images/pho.png",
      })
    }
    if (getApp().globalData.userInfo_detail.is_phone_login == 1) {
      this.setData({
        usertel: getApp().globalData.userInfo_detail.phone,
        username: getApp().globalData.userInfo_detail.username,
      })
    } else {
      this.setData({
        username: getApp().globalData.userInfo.nickName,
      })
    }
    if (getApp().globalData.userInfo_detail.main_balance != "" && getApp().globalData.userInfo_detail.second_balance != "") {
      var main = parseFloat(getApp().globalData.userInfo_detail.main_balance)
      var second = parseFloat(getApp().globalData.userInfo_detail.second_balance)
      var total = main + second
      var value = Math.round(parseFloat(total) * 100) / 100
      var xsd = value.toString().split(".")
      if (xsd.length == 1) {
        value = value.toString() + ".00";
        this.setData({
          balance: value,
        })
      }
      if (xsd.length > 1) {
        if (xsd[1].length < 2) {
          value = value.toString() + "0";
        }
        this.setData({
          balance: value,
        })
      }
      console.log(value)
    }
    if (getApp().globalData.userInfo_detail.is_phone_login == 1) {
      this.setData({
        is_phone_login: true
      })
    }

    //根据用户id得到其账户的剩余金额
    wx.request({
      url: getApp().globalData.server + '/API/User/get_balance_by_user_id',
      data: {
        shop_id: getApp().globalData.settings.shop_id,
        company_id: getApp().globalData.settings.company_id,
        user_id: getApp().globalData.userInfo_detail.user_id,
      },
      method: "POST",
      header: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      success: function(res) {
        console.log('根据用户id得到其账户的剩余金额', res.data)
        if (res.data.error_no != 0) {
          wx.showModal({
            title: '哎呀～',
            content: '出错了呢！' + res.data.data.error_msg,
            success: function(res) {
              if (res.confirm) {
                console.log('用户点击确定')
              } else if (res.cancel) {
                console.log('用户点击取消')
              }
            }
          })
        } else if (res.data.error_no == 0) {
          getApp().globalData.balance = res.data.data.total_balance

          var tem = "balance"
          that.setData({
            [tem]: res.data.data.total_balance
          })
        }
      },
      fail: function(res) {
        wx.showModal({
          title: '哎呀～',
          content: '网络不在状态呢！',
          success: function(res) {
            if (res.confirm) {
              console.log('用户点击确定')
            } else if (res.cancel) {
              console.log('用户点击取消')
            }
          }
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {
    return (util.share())
  },

  active: function() {
    if (getApp().globalData.userInfo_detail.is_phone_login == 0) {
      wx.navigateTo({
        url: '/pages/login/login'
      })
    } else if (getApp().globalData.userInfo_detail.is_phone_login == 1) {
      wx.showModal({
        title: '提示',
        content: '您已经登录！',
        showCancel: false,
        success: function(res) {
          if (res.confirm) {
            console.log('用户点击确定')
          } else if (res.cancel) {
            console.log('用户点击取消')
          }
        }
      })
    }
  },

  first_select: function() {
    if (getApp().globalData.settings.version == "1") {
      wx.redirectTo({
        url: '../index/index'
      })
    } else if (getApp().globalData.settings.version == "2") {
      wx.redirectTo({
        url: '../index/index'
      })
    } else if (getApp().globalData.settings.version == "3") {
      wx.redirectTo({
        url: '../another_index/another_index'
      })
    }
  },

  second_select: function() {
    wx.redirectTo({
      url: '/pages/service/service'
    })
  },

  routePage: function() {
    if (this.data.scene == '' || this.data.scene == undefined) {
      wx.redirectTo({
        url: '../index/index'
      })
    } else {
      //console.log(this.data.strs[0])
      switch (this.data.strs[0]) {
        case '1':
          {
            wx.redirectTo({
              url: '../payment/payment?goods_id=' + this.data.strs[1]
            })
          }
          break;
        default:
          {
            wx.redirectTo({
              url: '../index/index'
            })
          }
      }
    }
  },

  // pay: function () {
  //   wx.navigateTo({
  //     url: '../payment/payment'
  //   })
  // },

  about: function() {
    wx.navigateTo({
      url: '../about/about'
    })
  },

  aboutcell: function() {
    wx.navigateTo({
      url: '../seller/seller'
    })
  },

  daka: function() {
    wx.navigateTo({
      url: '../clock/clock'
    })
  },

  recha: function() {
    wx.navigateTo({
      url: '../recharge/recharge'
    })
  }
})